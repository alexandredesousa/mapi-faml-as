# mapi-faml-class

Project for the **MAP-i's Foundations and Applications of Machine Learning** course work,
developed by Alexandre Carneiro de Sousa.

# Project Structure
This project is structured as follows:

```
mapi-faml-class/
    data
    doc
        preliminary_analysis
    estimators
        classifiers
        feature_selectors
        preprocessors
    notebooks
    src
        adhoc_analysis
        feature_preprocessing
        modelling
        notebooks
        utils
    submission
    requirements.txt
    README.md
```

## data
The [data](data) directory is not tracked in git. It exists locally,
and if one intends to reproduce the results,
the file `santander-customer-transaction-prediction.zip` should be downloaded
from the competition website, and placed in this directory.
Due to data size, it is not uploaded.

## doc
The [doc](doc) dir is used for documentation of the project.

## estimators
The [estimators](estimators) dir is used to save the estimators (feature pre-processors, feature selectors, classifiers)
and avoid recomputation.

## notebooks
The [notebooks](notebooks) dir contains the jupyter notebooks used to perform some preliminary analysis.

## src
The [src](src) dir contains all the developed code. It is composed of the sub-directories:
* [data_viz_modelling](src/data_viz_modelling) - utility to perform some data visualizations.
* [feature_processing](src/feature_processing) - contains the pre-processing related tasks code.
* [modelling](src/modelling) - contains the modelling related tasks code.
* [utils](src/utils) - contains some general utility functions.

The files `experimentXYZ.py` are the scripts used to perform modelling.
More files are prepared than were executed.
Refer to [doc](doc), specifically to the results reported, to check which scripts have reported results.

## submission
Contains the files with the predictions submitted to the Kaggle platform for assessment.