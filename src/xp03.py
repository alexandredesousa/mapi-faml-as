from numpy import linspace, geomspace

from experiment_base import Experiment
import experiment_base_defs as xpdefs


###################
# preprocessing
###################
sampling = 'undersampling'
transformer = {
    'numeric_transformer': [['scaler'], ['robust'], []]
}

###################
# feature selection
###################
auto_selection = None

###################
# modelling
###################
classifiers = ['svm', 'lr', 'mlp', 'rf', 'gbt']

grid_params = {
    'svm__kernel': ('linear', 'rbf'),
    'svm__C': geomspace(1e-4, 1e1, 4),
    'ann__hidden_layer_sizes': [(20, 50, 15), (50, 10, 50), (10, 30, 10), (50, 50, 50)],
    'ann__activation': ['tanh', 'relu'],
    'ann__alpha': linspace(1e-4, 1e1, 5),
    'ann__solver': ['lbfgs'],
    'ann__max_iter': linspace(5e2, 1e4, 3),
    'rf__criterion': ['entropy', 'gini'],
    'rf__n_estimators': geomspace(5e1, 2e2, 3),
    'rf__max_depth': [10, 20, 50, 100, None],
    'rf__min_samples_split': geomspace(2e0, 1.6e1, 4),
    'rf__max_features': ['auto', 'sqrt', 'log2'],
    'gbt__learning_rate': geomspace(9e-4, 9e-1, 4),
    'gbt__n_estimators': geomspace(5e1, 2e2, 3),
    'gbt__max_depth': [3, 9, 20, 50, 100, None],
    'gbt__min_samples_split': geomspace(2e0, 1.6e1, 4),
    'gbt__max_features': ['auto', 'sqrt', 'log2']
}

###################
# identifier
###################
# note - the selection classifier is an optional parameter (so may not be in the dictionary defs).
# In that case, by default, uses svm. For the identifier we have to provide it manually at this stage
# Moreover, in case there is auto_selection, the experiment identifier is shorter
# (does not take all params for id as that leads to higher than allowed filenames when saving)
# - only takes the parameters selector, selection classifier and n features to select.
if bool(auto_selection):
    if auto_selection['estimator']:
        selection_classifier = auto_selection['estimator']
    else:
        selection_classifier = 'svm'
    identifier = 'preprocessed_{}_{}_{}_{}'.format(
        sampling,
        auto_selection['selector'] + '_' + str(auto_selection['search_space_selection'][0]['selector__n_features_to_select']),
        selection_classifier,
        classifiers
    )
else:
    selection_classifier = ''
    identifier = "preprocessed_{}_{}_{}_{}".format(sampling, auto_selection, selection_classifier, classifiers)

xp = Experiment(
    experiment_identifier=identifier,
    drop_columns=xpdefs.drop_columns, tgt_column=xpdefs.target_column,
    numerical_columns=xpdefs.numerical_columns, categorical_columns=xpdefs.categorical_columns, ordinal_columns=xpdefs.ordinal_columns,
    preprocessor=transformer,
    sampling_type=sampling,
    auto_selection=auto_selection,
    classifiers=classifiers, classifiers_search_space=grid_params,
    save_output=True
)