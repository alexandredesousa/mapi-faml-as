from datetime import datetime
import os.path

import pandas as pd
from sklearn.model_selection import train_test_split

from feature_processing import feature_engineering, sampling
from modelling import modelling
from utils import load_datasets, save_outputs


class Experiment:
    """
    Base to streamline the experiments performed.

    Using it one should be capable of:
      - load target data set.
      - perform variables pre-processing.
      - perform automatic feature selection.
      - perform classification (single model and ensemble).
      - perform model evaluation and selection.
      - perform prediction on the test set and prepare the submission file.
      - in every step, the fit estimators and/or resulting data sets can be saved.

    Notes:
    Pre-processing using FeatureEngineering class.
    Feature selection using FeatureSelection class.
    Modelling using Model class.

    Parameters
    ----------

    Returns
    -------

    See Also
    --------

    Examples
    --------

    Roadmap
    -------
    1) Load a dataset:
      - check if it exists (based on target experiment), or load original data set

    2) Create target data transformer:
      - depending on parameters, create a Pipeline chaining the required operations for
        sampling data, pre-processing, feature selection, feature reduction.
      - should only be performed if the data set does not already exist.
        if it does save computation power, and just load the processed data set

    3) Modelling:
      - train and evaluate the target classifiers

    Note:
    To save computation power, each computing step must save its estimators
    and/or computed data.
    It must follow a naming convention based on the experiment identifier, to allow
    for data or estimator loading in case they exist, by different instances.
    """
    # class variable - counter of number of experiments performed.
    N_EXPERIMENTS = 0
    SAVE_OUTPUT = True
    VALIDATION_SIZE = 0.3

    def __init__(self, experiment_identifier,
                 drop_columns, tgt_column,
                 numerical_columns, categorical_columns, ordinal_columns,
                 preprocessor,
                 classifiers, classifiers_search_space,
                 sampling_type=None,
                 auto_selection=None,
                 save_output=True):

        self.id = datetime.now().strftime('%Y%m%d_%H%M%S_{}'.format(experiment_identifier))
        self.times = {'experiment init time': datetime.now()}

        # Check if the data to model (according to preprocessor, sampling and selection)
        # already exists (previously computed).
        # In that case load it and save computation.
        # If previously computed, it is named according to the variables
        # preprocessor, sampling and selection.
        self.xp_identifier = self.get_dataset_identifier(experiment_identifier)
        dataset_name = 'train_transformed_data_' + self.xp_identifier
        dataset_name_test = 'test_transformed_data_' + self.xp_identifier
        if self.target_dataset_exists(dataset_name):
            self.df = load_datasets.load_dataset(dataset_name)
            self.df_test = load_datasets.load_dataset(dataset_name_test)
        else:
            self.df = load_datasets.load_base_datasets('train')['train']
            self.df_test = load_datasets.load_base_datasets('test')['test']

        self.drop_columns = drop_columns
        self.tgt_column = tgt_column
        # note: test data set does not have a target variable
        self.drop_columns_test = [d for d in drop_columns if d not in tgt_column]

        self.num_cols = numerical_columns
        self.cat_cols = categorical_columns
        self.ord_cols = ordinal_columns

        # Must be set at this stage to avoid data leakage between
        # validation and training set.
        self.X = self.df.drop(columns=self.drop_columns)
        self.y = self.df[tgt_column]

        # just to accelerate test computation
        # self.X = self.X.drop(index=range(800, 200000))
        # self.y = self.y.drop(index=range(800, 200000))

        self.X_train, self.X_validate, self.y_train, self.y_validate = train_test_split(
            self.X, self.y, test_size=Experiment.VALIDATION_SIZE, stratify=self.y)

        # To be set on call of the corresponding routines.
        self.pre_processing = None
        self.pre_processor = None
        self.feature_selection = None
        self.feature_selector = None
        self.data_transforming = None
        self.data_transformer = None

        # In case the data set already exists, both the preprocessor, the sampler and selector used to compute
        # the transformed data must also be available.
        if self.target_dataset_exists(dataset_name):
            # self.pre_processor = save_outputs.load_estimator('preprocessors', xp_identifier)
            # self.feature_selector = save_outputs.load_estimator('feature_selectors', xp_identifier)
            # Leave as None to assert it was not computed for this experiment.
            pass
        else:
            # If the data and preprocessors did not exist previously, compute them.
            # Initializing an empty DataTransformer:
            self.data_transformer = feature_engineering.DataTransformer()

            # Set the different transformers
            self.set_data_transformer(preprocessor, sampling_type, auto_selection, save_output)

            # Transform.
            if auto_selection is not None:
                if 'search_space_selection' in auto_selection.keys():
                    self.fit_data_transformer(auto_selection['search_space_selection'])
            else:
                self.fit_data_transformer()

        ###################
        # modelling
        ###################
        start_time_modelling = datetime.now()

        # Load the transformed data set to streamline the code
        # between those that need to compute,
        # and those that only require loading -
        # loads in both cases.

        # TODO: this is not entirely used. Change or delete.
        df = load_datasets.load_dataset('train_transformed_data_' + self.xp_identifier)
        df_test = load_datasets.load_dataset('test_transformed_data_' + self.xp_identifier)

        X = df.drop(columns=self.drop_columns)
        y = df[tgt_column]

        # just to accelerate test computation
        # X = X.drop(index=range(800, 200000))
        # y = y.drop(index=range(800, 200000))

        self.X_train, self.X_validate, self.y_train, self.y_validate = train_test_split(
            X, y, test_size=Experiment.VALIDATION_SIZE, stratify=y
        )

        # set model parameters in the class
        self.preds = []
        self.metrics = []
        self.best_results = []
        self.best_estimators = []
        self.best_params = []
        self.estimator_times = []
        self.best_score = None
        self.best_estimator = None

        modelling_msg = "****************** GRID SEARCH ******************\n"
        for i, clf in enumerate(classifiers):
            # no transformation to perform (using transformed data set already transformed, computed in the pipe above):
            params = classifiers_search_space[clf] if \
                classifiers_search_space is not None and clf in classifiers_search_space else None

            self.search_classifier(clf, params)
            msg = (
                "****************** {}\n"
                "Metrics:\n{}\nBest params:\n{}\nRun time:{}\n"
            ).format(
                clf,
                self.metrics[-1], self.best_params[-1], self.estimator_times[-1]
            )
            modelling_msg += msg

        best_index = self.pick_best_classifier(target_metric_index=-1)
        msg_best = (
            "****************** Selected best individual classifier\n"
            "Index: {}\nScore: {}\nClass: {}\n"
        ).format(
            best_index, self.best_score, classifiers[best_index]
        )
        modelling_msg += msg_best

        print(modelling_msg)
        if save_output:
            save_outputs.save_output_file(modelling_msg, 'modelling_info_{}'.format(self.id), '../doc')

        self.times['global_modelling_times'] = datetime.now() - start_time_modelling
        self.times['local_modelling_times'] = self.estimator_times

        ###################
        # prediction
        ###################
        # predict with best model.
        # train best performer on the whole train data set.
        # test on the test data set (only now used)
        self.best_estimator.fit(self.X, self.y)

        # only now setting the test data:
        self.X_test = self.df_test.drop(columns=self.drop_columns_test)
        # just to accelerate test computation
        # self.df_test.drop(index=range(800, 200000), inplace=True)
        # self.X_test = self.X_test.drop(index=range(800, 200000))

        preds_best_classifier = self.best_estimator = self.best_estimator.predict(self.X_test)

        # submission:
        if save_output:
            save_outputs.prepare_submission(self.df_test.iloc[:, 0],
                                            preds_best_classifier,
                                            filename=self.id)

        save_outputs.save_estimator(self.best_estimator, 'classifiers', self.xp_identifier)

        self.times['experiment finish time'] = datetime.now()
        self.times['experiment run time'] = self.times['experiment finish time'] - self.times['experiment init time']

    @staticmethod
    def get_dataset_identifier(experiment_identifier):
        classifiers_id = experiment_identifier.split('_')[-1]
        if experiment_identifier.endswith(classifiers_id):
            return experiment_identifier[:-(len(classifiers_id)+1)]
        else:
            # it probably does not make sense. In what case will it come this path?
            raise ValueError('It was not possible to remove the suffix from the identifier')

    @staticmethod
    def target_dataset_exists(target_dataset_name):
        """
        type: should be either 'train' or 'test'
        """
        return os.path.isfile('../data/{}.csv'.format(target_dataset_name))

    def set_data_transformer(self, preprocessor, sampling_type=None, auto_selection=None, save_output=True):
        # sampling does not enter a Transformer, it is computed independently
        if sampling_type is not None:
            self.sampler(sampling_type)

        # add pre-processor to the DataTransform object
        # self.preprocessing(**preprocessor)
        self.preprocessing(preprocessor)
        self.data_transformer.add_steps(('pre-processor', self.pre_processor))

        if auto_selection is not None:
            self.auto_feature_selection(**auto_selection)
            self.data_transformer.add_steps(('selector', self.feature_selector))
            self.data_transformer.add_steps(('classifier', self.feature_selection.estimator))

    def fit_data_transformer(self, search_space_selection=None, save_output=False):
        # refactor this and remove search_space_selection as parameter
        data_transform_init_time = datetime.now()
        self.data_transformer.fit(self.X_train, self.y_train, search_space_selection)
        self.times['data transform fit run time'] = datetime.now() - data_transform_init_time

        if search_space_selection is not None:
            # Specific in case there is auto feature selection.
            # If there is an auto feature selector it must be fit (using the given classifier), and the data
            # transformed. Then a different classifier may be used for the classification task.
            # Drop classifier step from pipeline. Only want the feature selection part of the pipeline.
            feature_selector = self.data_transformer.grid_selector.best_estimator_
            feature_selector.steps.pop(len(feature_selector.steps) - 1)
            selected_features_indices = feature_selector.named_steps['selector'].support_
            selected_features_n = feature_selector.named_steps['selector'].n_features_to_select_
            selection_results = pd.DataFrame(self.data_transformer.grid_selector.cv_results_)
            selection_results = selection_results.sort_values("mean_test_score", ascending=False)
            selected_features_ids = [v for (k, v) in list(zip(selected_features_indices, list(self.X_train.columns.values)))
                                     if k]

            if save_output:
                msg = (
                    "****************** BACKWARD ELIMINATION (AUTO) ******************\n"
                    "Number of features selected: {}\n"
                    "Selected features indices:\n{}\n"
                    "Selected features ids:\n{}\n"
                    "Grid results:\n{}\n"
                    "Run time: {}"
                ).format(
                    selected_features_n,
                    selected_features_indices,
                    selected_features_ids,
                    selection_results,
                    self.times['selection run time']
                )

                save_outputs.save_output_file(msg, 'backward_elimination_auto_output', '../doc')

        # save the transformed df and transformer estimator:
        transformer_estimator_identifier = 'transformed_data_{}'.format(self.xp_identifier)
        estimator_type = 'feature_selectors' if search_space_selection is not None else 'preprocessors'
        save_outputs.save_estimator(self.data_transformer.grid_selector, estimator_type, self.xp_identifier)

        save_features = selected_features_ids if search_space_selection is not None else self.X_train.columns
        X_transformed = self.transform_dataset(
            self.data_transformer.grid_selector.best_estimator_, self.df, save_features, self.drop_columns
        )
        save_outputs.save_dataframes(X=X_transformed, y=self.df[self.tgt_column],
                                     filename='train_transformed_data_' + self.xp_identifier)

        # target column not used to save in the test data frame
        save_features_test = [sf for sf in save_features if sf not in self.tgt_column]

        X_transformed_test = self.transform_dataset(
            self.data_transformer.grid_selector.best_estimator_, self.df_test, save_features_test, self.drop_columns_test
        )
        save_outputs.save_dataframes(X=X_transformed_test, filename='test_transformed_data_' + self.xp_identifier)

    def preprocessing(self, preprocessor_dict):
        if 'numeric_transformer' in preprocessor_dict:
            # preprocessor_dict['numeric_transformer'][2].append(self.num_cols)
            preprocessor_dict['numeric_transformer'][2].extend(self.num_cols)
        self.pre_processor = feature_engineering.set_cols_transformer(preprocessor_dict)

    def auto_feature_selection(self, n_features, scoring, selector, direction, cv=5, n_jobs=-1, estimator=None, search_space_selection=None):
        slct_clf = modelling.Classifiers(estimator).clf if estimator is not None else modelling.Classifiers_sk.svm.value
        self.feature_selection = feature_engineering.FeaturesSelection(
            n_features_to_select=n_features, scoring=scoring,
            selector=selector, direction=direction,
            cv=cv, n_jobs=n_jobs,
            estimator=slct_clf
        )
        self.feature_selector = self.feature_selection.selector

    def sampler(self, sampling_type):
        if sampling_type == 'oversampling':
            sampling.oversampler(self.X, self.y)
        elif sampling_type == 'undersampling':
            sampling.undersampler(self.X, self.y)
        else:
            raise ValueError(
                'Only \'undersampling\' and \'oversampling\' methods are available. Got {}'.format(sampling_type)
            )

    def transform_dataset(self, transformer, df, selected_features_ids, drop_columns):
        X_transformed = df.drop(columns=drop_columns)
        # X_transformed = pd.DataFrame(transformer.transform(X_transformed).toarray(), columns=selected_features_ids)
        X_transformed = pd.DataFrame(transformer.transform(X_transformed), columns=selected_features_ids)
        # TODO: get from df directly rather than hard coding
        X_transformed.insert(0, 'ID_code', df.iloc[:, 0])

        return X_transformed

    def search_classifier(self, clf, search_params):
        start_time_modelling = datetime.now()
        model_i = modelling.ModelIntegrated(classifier_str=clf)
        model_i.fit(self.X_train, self.y_train, search_params)
        model_i.evaluate(clf, self.X_validate, self.y_validate)

        self.preds.append(model_i.tuner.predict(self.X_validate))
        self.metrics.append(model_i.metrics)
        self.best_results.append(pd.DataFrame(model_i.tuner.cv_results_))
        self.best_estimators.append(model_i.tuner.best_estimator_)
        self.best_params.append(model_i.tuner.best_params_)

        self.estimator_times.append(datetime.now() - start_time_modelling)

        print('Model {}: {}'.format(clf, self.metrics[-1]))

    def pick_best_classifier(self, target_metric_index=0):
        # target_metric = [[v[target_metric_index] for k, v in class_metrics.items()] for class_metrics in self.metrics]
        # previous retrieves list of list. Following, retrieves list of best results (as expected)
        target_metric = [v[target_metric_index] for class_metrics in self.metrics for k, v in class_metrics.items()]
        max_score_index = target_metric.index(max(target_metric))

        self.best_score = target_metric[max_score_index]
        self.best_estimator = self.best_estimators[max_score_index]

        return max_score_index
