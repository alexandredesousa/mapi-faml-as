"""
Replicates experiment001 using keras

Loads pre-processed data sets - see experiment000.py

experiment: undersampling
train data: train_preprocessed_std_undersampling
test data: test_preprocessed_std
"""
if __name__ == "__main__":
    from datetime import datetime

    import pandas as pd
    import numpy as np
    from sklearn.model_selection import train_test_split, GridSearchCV, RandomizedSearchCV
    from sklearn.utils.fixes import loguniform

    from src.modelling import modelling, evaluation, classifiers_dnn
    from src.utils import load_datasets, save_outputs

    base_dir = '..'
    save_output = True

    preprocessing = 'preprocessed_std'
    sampling = 'undersampling'
    pivoting = None
    identifier = f'_{preprocessing}'
    if sampling is not None:
        identifier += f'_{sampling}'
    if pivoting is not None:
        identifier += f'_{pivoting}'
    experiment_id = f'exp006{identifier}'

    start_time = datetime.now()

    ###### modelling
    start_time_modelling = datetime.now()

    df = load_datasets.load_dataset(f'train{identifier}', base_dir=base_dir, mode='parquet')
    X = df.drop(columns=['ID_code', 'target'])
    y = df['target']

    # just to accelerate test computation
    X = X.take(np.random.randint(df.shape[0], size=200))
    y = y.take(np.random.randint(df.shape[0], size=200))

    ## crete train and validation data sets (uses the df computed previously)
    validation_size = 0.3
    X_train, X_validate, y_train, y_validate = train_test_split(X, y, test_size=validation_size)
    y_train = y_train.to_numpy().reshape(y_train.shape[0], 1)
    y_validate = y_validate.to_numpy().reshape(y_validate.shape[0], 1)

    # no transformation to perform (using transformed data set already transformed, computed in the pipe above):
    # define some classifiers to use in the ensemble

    def search_dnn_classifier(opt_params):
        best_config, best_val_acc = classifiers_dnn.dnn_optimization(opt_params, X_train, y_train, X_validate, y_validate, 20)
        best_model = classifiers_dnn.setup_model(best_config[0], best_config[1], X_train.shape[1], y_train.shape[1])
        best_model = classifiers_dnn.train_dnn(best_model, best_config[2], best_config[3], X_train, y_train)

        # see:
        # https://machinelearningmastery.com/how-to-calculate-precision-recall-f1-and-more-for-deep-learning-models/
        # https://stackoverflow.com/questions/68836551/keras-attributeerror-sequential-object-has-no-attribute-predict-classes
        model_score = best_model.predict(X_validate, verbose=0)
        model_pred = np.argmax(model_score, axis=1)


        metrics = evaluation.get_metrics(y_validate, model_pred, model_score)

        return model_pred, metrics, 'best_results', best_model, best_config, opt_params


    # dnn
    opt_pars = {"topology": [[100], [100, 50], [250], [250, 100]],
                "algorithm": ["adam", "rmsprop", "sgd_momentum"],
                "lr": [0.01, 0.001],
                "dropout": [0, 0.2, 0.5]}

    dnn_model_pred, dnn_metrics, dnn_best_results, dnn_best_estimator, dnn_best_params, dnn_searched_params = \
        search_dnn_classifier(opt_pars)

    print(f'DNN: {dnn_metrics}')

    time_dnn_end = datetime.now()
    run_time_dnn = time_dnn_end - start_time_modelling

    msg = (
        f"****************** GRID SEARCH ******************\n"
        f"****************** DNN"
        f"Metrics:\n{dnn_metrics}\nBest params:\n{dnn_best_params}\nseached params:\n{dnn_searched_params}\nRun time:{run_time_dnn}"
    )

    print(msg)
    if save_output:
        save_outputs.save_output_file(msg, f'{experiment_id}_results_grid', '../doc')

    # train individual best parameters in the whole training data set:
    dnn_best_estimator.fit(X, y)

    df_test = load_datasets.load_dataset('test_preprocessed_std', base_dir=base_dir, mode='parquet')
    X_test = df_test.drop(columns=['ID_code'])

    preds_best_classifier_score = dnn_best_estimator.predict(X_test, verbose=0)
    preds_best_classifier = np.argmax(preds_best_classifier_score, axis=1)

    # save submission
    save_outputs.prepare_submission(df_test.iloc[:, 0], preds_best_classifier,
                                    filename=f'{experiment_id}_submission_grid_best_classifier_auto', base_dir=base_dir)
    # save classifiers
    save_outputs.save_estimator(dnn_best_estimator, 'classifiers', f'{experiment_id}_grid_auto_selected_best', f'{base_dir}/estimators')
