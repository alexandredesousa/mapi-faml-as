from src.utils import save_outputs


added_timestamp = '2021-12-03 14:47:06.161127'
exec_init_timestamp = '2021-12-03 14:47:06.161127'
model = 'svm'
id_metric = 1
value = 0.1821
exec_fin_timestamp = '2021-12-03 14:47:06.161127'
notes = ''

for i in range(5):
    msg_dict = {
        'id_exec': i,
        'added_timestamp': added_timestamp,
        'exec_init_timestamp': exec_init_timestamp,
        'model': model,
        'id_metric': id_metric,
        'value': value,
        'exec_fin_timestamp': exec_fin_timestamp,
        'notes': notes
    }

    save_outputs.save_viz_record(msg_dict)

for i in range(5, 10):
    msg_dict = {
        'id_exec': i,
        'added_timestamp': added_timestamp,
        'exec_init_timestamp': exec_init_timestamp,
        'model': model,
        'id_metric': id_metric,
        'value': value,
        'exec_fin_timestamp': exec_fin_timestamp,
        'notes': notes
    }

    save_outputs.save_viz_record(msg_dict)