from datetime import datetime
from random import randint, random

import pandas as pd

from viz_defs import METRICS_PD

RANGE_PREDICTS_0 = [0, 179902]
RANGE_PREDICTS_1 = [0, 20098]

def create_init_df():
    record_0 = [[1, datetime.now(), datetime.now(), 'lr', 1, 0.691,  datetime.now()]]
    df = pd.DataFrame(
        record_0, columns=['id_exec', 'added_timestamp', 'exec_init_timestamp', 'model', 'id_metric', 'value',
                          'exec_fin_timestamp']
    )
    df.to_csv('randomly_generated_data.csv', index=False)


def populate_df_randomly(execs=1, models=['lr', 'svm', 'knn']):
    records = []

    current_exec = pd.read_csv('randomly_generated_data.csv').iloc[-1]['id_exec']

    for i in range(current_exec + 1, current_exec + (execs + 1)):
        exec_id = i

        for j, model in enumerate(models):

            for k, metric in enumerate(range(METRICS_PD.shape[0])):
                current_metric_name = METRICS_PD.iloc[k]['metric']
                current_metric_id = METRICS_PD.iloc[k]['id_metric']

                if current_metric_name == 'predicts_1':
                    metric_value = randint(RANGE_PREDICTS_1[0], RANGE_PREDICTS_1[1])
                elif current_metric_name == 'predicts_0':
                    metric_value = randint(RANGE_PREDICTS_0[0], RANGE_PREDICTS_0[1])
                else:
                    metric_value = round(random(), 3)

                record = [
                    exec_id,
                    datetime.now(),
                    datetime.now(),
                    model,
                    current_metric_id,
                    metric_value,
                    datetime.now()
                ]

                records.append(record)

    df = pd.DataFrame(
        records)
    df.to_csv('randomly_generated_data.csv', header=False, index=False, mode='a')


create_init_df()
populate_df_randomly(100)