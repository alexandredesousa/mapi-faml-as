import pandas as pd
import numpy as np
from datetime import datetime

from pandas.core.frame import DataFrame

# metrics id
metrics_id = np.array(
    (1, 2, 3, 4, 5, 6)
)

# metrics named identifier
metrics_name = np.array(
    ('accuracy',
    'precision',
    'recall',
    'f1_score',
    'predicts_1',
    'predicts_0'
    )
)

# date metric was added
metrics_added_date = np.array(
    (datetime(2021, 12, 2),
    datetime(2021, 12, 2),
    datetime(2021, 12, 2),
    datetime(2021, 12, 2),
    datetime(2021, 12, 2),
    datetime(2021, 12, 2)
    )
)

metrics_data = np.array((metrics_id, metrics_name, metrics_added_date))

METRICS_PD = DataFrame(metrics_data.T, columns=['id_metric', 'metric', 'date_added'])

METRICS_PD.to_csv('metrics_identification.csv', index=False)