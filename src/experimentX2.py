"""
script to reproduce the preliminary analysis notebook modelling code.
"""

from experiment_base import Experiment
import experiment_base_defs as xpdefs


###################
# preprocessing
###################
# sampling = 'oversampled'
sampling = None
transformer = {
    'numeric_transformer': [['scaler'], ['standard'], []]
}

###################
# feature selection
###################
auto_selection = None

###################
# modelling
###################
classifiers = ['lr', 'knn', 'svm']
grid_params = None

###################
# identifier
###################
# note - the selection classifier is an optional parameter (so may not be in the dictionary defs).
# In that case, by default, uses svm. For the identifier we have to provide it manually at this stage.
# Moreover, in case there is auto_selection, the experiment identifier is shorter
# (does not take all params for id as that leads to higher than allowed filenames when saving)
# - only takes the parameters selector, selection classifier and n features to select.
if bool(auto_selection):
    if auto_selection['estimator']:
        selection_classifier = auto_selection['estimator']
    else:
        selection_classifier = 'svm'
    identifier = 'preprocessed_{}_{}_{}_{}'.format(
        sampling,
        auto_selection['selector'] + '_' + str(auto_selection['search_space_selection'][0]['selector__n_features_to_select']),
        selection_classifier,
        classifiers
    )
else:
    selection_classifier = ''
    identifier = "preprocessed_{}_{}_{}_{}".format(sampling, auto_selection, selection_classifier, classifiers)

xp = Experiment(
    experiment_identifier=identifier,
    drop_columns=xpdefs.drop_columns, tgt_column=xpdefs.target_column,
    numerical_columns=xpdefs.numerical_columns, categorical_columns=xpdefs.categorical_columns, ordinal_columns=xpdefs.ordinal_columns,
    preprocessor=transformer,
    sampling_type=sampling,
    auto_selection=auto_selection,
    classifiers=classifiers, classifiers_search_space=grid_params,
    save_output=True
)