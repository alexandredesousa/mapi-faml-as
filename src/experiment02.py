"""
See Also
--------
https://stackoverflow.com/questions/419163/what-does-if-name-main-do
"""
if __name__ == "__main__":
    from datetime import datetime
    from tempfile import mkdtemp
    from shutil import rmtree
    from joblib import Memory

    import pandas as pd
    from sklearn.pipeline import Pipeline
    from sklearn.model_selection import train_test_split, GridSearchCV
    from sklearn.ensemble import VotingClassifier

    from feature_processing import feature_engineering
    from modelling import modelling, evaluation
    from utils import load_datasets, save_outputs

    start_time = datetime.now()

    base_dir = ".."
    # load dataframes (returns a dictionary with both the train and test data sets,as pandas dataframe).
    dfs = load_datasets.load_base_datasets('all')
    df = dfs['train']
    save_output = False

    # Create a temporary folder to store the transformers of the pipeline
    cachedir = mkdtemp()
    memory = Memory(location=cachedir, verbose=1)

    try:
        X = df.drop(columns=['ID_code', 'target'])
        y = df['target']

        # just to accelerate test computation
        X = X.drop(index=range(800, 200000))
        y = y.drop(index=range(800, 200000))


        clf_selection = modelling.Classifiers_sk.svm.value
        sfs = feature_engineering.FeaturesSelection(0.99).selector

        engineered_data = feature_engineering.FeatureEngineering(list(X.columns.values), [], [], False)

        pipeline_selection = Pipeline([
            ('preprocessing', engineered_data.preprocessor),
            ('feature_selection', sfs),
            ('classifier', clf_selection)
        ], memory=memory)

        search_space_selection = [
            {'feature_selection__n_features_to_select': [0.99]}
        ]

        grid_selector = GridSearchCV(pipeline_selection, search_space_selection, cv=3, n_jobs=-1, verbose=3,
                                     scoring='roc_auc')
        grid_selector.fit(X, y)

        time_feature_selection_end = datetime.now()
        run_time_feature_selection = time_feature_selection_end - start_time

        feature_selector = grid_selector.best_estimator_
        feature_selector.steps.pop(len(feature_selector.steps) - 1)
        selected_features_indices = feature_selector.named_steps['feature_selection'].support_
        selected_features_n = feature_selector.named_steps['feature_selection'].n_features_to_select_

        selection_results = pd.DataFrame(grid_selector.cv_results_)
        selection_results = selection_results.sort_values("mean_test_score", ascending=False)

        msg = (
            "****************** BACKWARD ELIMINATION (AUTO) ******************\n"
            "Number of features selected: {}\n"
            "Selected features indices (variables dict):\n{}\n"
            "Grid results:\n{}\n"
            "Run time: {}"
        ).format(
            selected_features_n,
            selected_features_indices,
            selection_results,
            run_time_feature_selection
        )

        print(msg)

        if save_output:
            save_outputs.save_output_file(msg, 'backward_elimination_auto_output', '../doc')
            save_outputs.save_estimator_pckl(grid_selector, 'pipeline_grid_selection', 'svm')
            save_outputs.save_estimator(grid_selector, 'feature_selectors', 'pipeline_grid_selection_svm')

        # save the feature selected df
        # train data
        selected_df = df.copy()
        X_selected = selected_df.drop(columns=['ID_code', 'target'])
        y_selected = selected_df['target']
        #X_selected = pd.DataFrame(feature_selector.transform(X_selected).toarray())
        X_selected = pd.DataFrame(feature_selector.transform(X_selected))
        # ids_selected = selected_df.iloc[:, 0]
        # X_selected.insert(0, 'ID_code', ids_selected)
        # save_outputs.save_dataframes(X_selected, y_selected, 'train_preprocessed_feature_drop_auto')

        # test data
        # df_test = pd.read_csv(base_dir + '/data/test.csv')
        df_test = dfs['test']
        X_selected_test = df_test.drop(columns=['ID_code'])
        # y_selected_test = df_test['target']
        # X_selected_test = pd.DataFrame(feature_selector.transform(X_selected_test).toarray())
        X_selected_test = pd.DataFrame(feature_selector.transform(X_selected_test))
        # ids_selected_test = df_test.iloc[:, 0]
        # X_selected_test.insert(0, 'ID_code', ids_selected_test)
        # save_outputs.save_dataframes(X_selected_test, y_selected_test, 'test_preprocessed_feature_drop_auto')
    except Exception as e:
        print(e)
    finally:
        # delete the temporary cache before exiting
        rmtree(cachedir)

    ###### modelling
    start_time_modelling = datetime.now()

    ## crete train and validation data sets (uses the df computed above)
    # df = pd.read_csv(base_dir + '/data/train_preprocessed_feature_drop_auto.csv')
    X = X_selected.drop(index=range(800, 200000))
    y = y_selected.drop(index=range(800, 200000))

    validation_size = 0.3
    X_train, X_validate, y_train, y_validate = train_test_split(X, y, test_size=validation_size, stratify=y)

    # no transformation to perform (using transformed data set already transformed, computed in the pipe above):
    # define some classifiers to use in the ensemble

    def search_classifier(clf, grid_params):
        classifier = GridSearchCV(
            estimator=clf,
            param_grid=grid_params,
            scoring='accuracy',
            n_jobs=-1,
            return_train_score=True
        )
        classifier.fit(X_train, y_train)
        model_pred = classifier.predict(X_validate)
        metrics = evaluation.get_metrics(y_validate, model_pred)
        best_results = pd.DataFrame(classifier.cv_results_)
        best_results = best_results.sort_values("mean_test_score", ascending=False)
        best_estimator = classifier.best_estimator_
        best_parameters = classifier.best_params_

        return model_pred, metrics, best_results, best_estimator, best_parameters


    # svm
    clf_svm = modelling.Classifiers_sk.svm.value
    svm_grid_params = {
        'kernel': ('linear', 'rbf'),
        'C': (0.01, 0.1, 1, 10)
    }
    svc_model_pred, svc_metrics, svc_best_results, svc_best_estimator, svc_best_params = search_classifier(clf_svm,
                                                                                                           svm_grid_params)

    print('SVC: {}'.format(svc_metrics))

    time_svm_end = datetime.now()
    run_time_svm = time_svm_end - start_time_modelling

    # lr
    clf_lr = modelling.Classifiers_sk.lr.value
    lr_grid_params = {}
    lr_model_pred, lr_metrics, lr_best_results, lr_best_estimator, lr_best_params = search_classifier(clf_lr,
                                                                                                      lr_grid_params)

    print('LR: {}'.format(lr_metrics))

    time_lr_end = datetime.now()
    run_time_lr = time_lr_end - time_svm_end


    # ensemble of the classifiers
    def search_ensemble(clf, grid_params):
        classifier = GridSearchCV(
            estimator=clf,
            param_grid=grid_params,
            scoring='accuracy',
            n_jobs=-1,
            return_train_score=True
        )
        classifier.fit(X_train, y_train)
        model_pred = classifier.predict(X_validate)
        metrics = evaluation.get_metrics(y_validate, model_pred)
        best_results = pd.DataFrame(classifier.cv_results_)
        best_results = best_results.sort_values("mean_test_score", ascending=False)
        best_estimator = classifier.best_estimator_
        best_parameters = classifier.best_params_

        return model_pred, metrics, best_results, best_estimator, best_parameters


    eclf = VotingClassifier(
        estimators=[
            ('svm', clf_svm),
            ('lr', clf_lr)
        ],
        voting='hard')

    ensemble_grid_params = {
        'svm__kernel': ('linear', 'rbf'),
        'svm__C': (0.01, 0.1, 1, 10),
    }

    eclf_model_pred, eclf_metrics, eclf_best_results, eclf_best_estimator, eclf_best_params = search_ensemble(eclf,
                                                                                                              ensemble_grid_params)

    print('Ensemble: {}'.format(eclf_metrics))

    time_ensemble_end = datetime.now()
    run_time_ensemble = time_ensemble_end - time_lr_end

    # set best classifier (considering accuracy):
    best_classifiers = [svc_best_estimator, lr_best_estimator]
    best_accuracies = [svc_metrics[0], lr_metrics[0]]
    max_score_index = best_accuracies.index(max(best_accuracies))
    best_score = best_accuracies[max_score_index]
    best_classifier = best_classifiers[max_score_index]

    msg = (
        "****************** GRID SEARCH ******************\n"
        "****************** SVC"
        "Metrics:\n{}\nBest params:\n{}\nRun time:{}"
        "****************** LR"
        "Metrics LR:\n{}\nBest params:\n{}\nRun time:{}"
        "****************** Ensemble"
        "Metrics Ensemble:\n{}\nBest params:\n{}\nRun time:{}"
        "****************** Selected best individual classifier"
        "Index: {}\nScore: {}\nClass: {}\n"
    ).format(
        svc_metrics, svc_best_params, run_time_svm,
        lr_metrics, lr_best_params, run_time_lr,
        eclf_metrics, eclf_best_params, run_time_ensemble,
        max_score_index, best_score, best_classifier
    )

    print(msg)
    if save_output:
        save_outputs.save_output_file(msg, 'results_grid_class_and_ensemble_preprocessed_data_feature_drop', '../doc')

    # train individual best performer and ensemble in the whole training data set:
    best_classifier.fit(X, y)
    eclf_best_estimator.fit(X, y)

    X_test = X_selected_test
    # y_test = y_selected_test

    preds_best_classifier = best_classifier.predict(X_test)

    # predict with ensemble classifier
    preds_ensemble = eclf_best_estimator.predict(X_test)

    # submission
    if save_output:
        save_outputs.prepare_submission(df_test.iloc[:, 0], preds_best_classifier,
                                        filename='submission_grid_best_classifier_auto')
        save_outputs.prepare_submission(df_test.iloc[:, 0], preds_ensemble,
                                        filename='submission_grid_ensemble_classifier_auto')

    # save classifiers
    if save_output:
        # save_outputs.save_estimator_pckl(best_classifier, 'grid_auto', 'selected_best')
        save_outputs.save_estimator(best_classifier, 'classifiers', 'grid_auto_selected_best')
        # save_outputs.save_estimator_pckl(eclf_best_estimator, 'grid_auto', 'ensemble')
        save_outputs.save_estimator(best_classifier, 'classifiers', 'grid_auto_ensemble')

    # test if save file and pickle are ok
    save_outputs.prepare_submission(df_test.iloc[:, 0], preds_best_classifier,
                                    filename='submission_grid_best_classifier_auto')
    save_outputs.save_estimator_pckl(best_classifier, 'grid_auto', 'selected_best')
    save_outputs.save_estimator(best_classifier, 'classifiers', 'grid_auto_selected_best')
