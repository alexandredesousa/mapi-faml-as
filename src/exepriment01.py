"""
See Also
--------
https://stackoverflow.com/questions/419163/what-does-if-name-main-do
"""
if __name__ == "__main__":
    from datetime import datetime
    from sklearn.model_selection import train_test_split

    from feature_processing import feature_engineering
    from modelling import modelling, evaluation
    from utils import load_datasets

    start_time = datetime.now()

    base_dir = ".."
    seed = 101101011
    save_output = False
    # load dataframes (returns a dictionary with both the train and test data sets,as pandas dataframe).
    dfs = load_datasets.load_base_datasets('all')
    df = dfs['train']

    X = df.drop(columns=['ID_code', 'target'])
    y = df['target']

    # just to accelerate test computation
    X = X.drop(index=range(800, 200000))
    y = y.drop(index=range(800, 200000))

    test_size = 0.3

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=seed, stratify=y)

    engineered_data = feature_engineering.FeatureEngineering(list(X.columns.values), [], [], False)

    model = modelling.Model('lr', engineered_data.preprocessor)
    # test with params
    # model = modelling.Model('lr', engineered_data.preprocessor, {'C': 3.0})

    model.fit(X_train, y_train)

    model_pred = model.full_pipeline.predict(X_test)

    metrics = evaluation.get_metrics(y_test, model_pred)

    print(round(metrics[0], 3), round(metrics[1], 3), round(metrics[2], 3), round(metrics[3], 3))