import pickle
import pandas as pd
import matplotlib.pyplot as plt
import csv

BASE_DIR = ".."
BASE_OUTPUT_DIR = BASE_DIR + "/doc"


def save_output_file(msg, filename, save_dir=None, fileextension='.txt', override=False):
    write_mode = 'w' if override else 'a'
    if save_dir is None:
        output_file = BASE_OUTPUT_DIR + '/' + filename + fileextension
    else:
        output_file = save_dir + '/' + filename + fileextension

    with open(output_file, write_mode) as f:
        print(msg, file=f)


def save_output_plots(pltname, save_dir=None, pltextension=".svg"):
    if save_dir is None:
        plot_dir = BASE_OUTPUT_DIR + '/plots/' + pltname + pltextension
    else:
        plot_dir = save_dir + '/' + pltname + pltextension

    plt.savefig(plot_dir, transparent=True, bbox_inches='tight')


def save_dataframes(X, filename, y=None, base_dir=BASE_DIR, mode='csv'):
    if y is not None:
        # add_col = {y.columns.values[0]: y}
        add_col = {y.name: y}
        df = X.assign(**add_col)
    else:
        df = X
    # df.to_csv(path_or_buf=dir + '/data/' + filename + '.csv', index=False)

    if mode == 'csv':
        df.to_csv(path_or_buf=f'{base_dir}/data/{filename}.csv', index=False)
    elif mode == 'pickle':
        df.to_pickle(path=f'{base_dir}/data/{filename}.pkl')
    elif mode == 'parquet':
        df.to_parquet(path=f'{base_dir}/data/{filename}.gzip', compression='gzip')
    elif mode == 'feather':
        df.to_feather(path=f'{base_dir}/data/{filename}.ftr')
    else:
        raise KeyError(f'Mode argument \'mode={mode}\' not available. '
                       f'Expected one of (csv, pickle, parquet, feather).')


def save_viz_record(record_dict, filename='data_viz.csv', base_dir=BASE_DIR):
    # df.to_csv('randomly_generated_data.csv', header=False, index=False, mode='a')
    if base_dir == BASE_DIR:
        filepath = base_dir + "/src/data_viz_modelling/" + filename
    else:
        filepath = base_dir + "/" + filename
    with open(filepath, 'a', newline='') as f:
        # using csv.writer method from CSV package
        # write = csv.writer(f)
        # write.writerow(fields)
        # write.writerows(record)
        writer = csv.DictWriter(f, fieldnames=record_dict.keys())
        writer.writerow(record_dict)


def save_estimator_pckl(clf, model_type, model_id, relative_models_dir='../models/'):
    with open(f"{relative_models_dir}{model_type}_{model_id}.pkl", 'wb') as f:
        pickle.dump(clf, f)


def load_estimator_pckl(model_type, model_id, relative_models_dir='../../models/'):
    with open("{}{}_{}.pkl".format(relative_models_dir, model_type, model_id), 'rb') as f:
        return pickle.load(f)


def save_estimator(estimator, estimator_type, identifier, relative_estimators_dir='../estimators'):
    with open(f"{relative_estimators_dir}/{estimator_type}/{identifier}.pkl", 'wb') as f:
        pickle.dump(estimator, f)


def load_estimator(estimator_type, identifier, relative_estimators_dir='../estimators'):
    with open("{}/{}/{}.pkl".format(relative_estimators_dir, estimator_type, identifier), 'rb') as f:
        return pickle.load(f)


def prepare_submission(ids, predictions, filename='submission', base_dir=BASE_DIR):
    subm = pd.DataFrame(data={'ID_code': ids, 'target': predictions})
    subm.to_csv(path_or_buf= f'{base_dir}/submission/{filename}.csv', index=False)
    return subm
