import pandas as pd
from zipfile import ZipFile

# deprecate var. Use as parameter in call function instead
BASE_DIR = ".."


def load_dataset(dataset_identifier, base_dir=BASE_DIR, mode='csv'):
    """

    """
    if mode == 'csv':
        return pd.read_csv(base_dir + '/data/{}.csv'.format(dataset_identifier))
    elif mode == 'pickle':
        return pd.read_pickle(base_dir + '/data/{}.pkl'.format(dataset_identifier))
    elif mode == 'parquet':
        return pd.read_parquet(base_dir + '/data/{}.gzip'.format(dataset_identifier))
    elif mode == 'feather':
        return pd.read_feather(base_dir + '/data/{}.ftr'.format(dataset_identifier))
    else:
        raise KeyError(f'Mode argument \'mode={mode}\' not available. '
                       f'Expected one of (csv, pickle, parquet, feather).')


def load_base_datasets(which='train', base_dir=BASE_DIR):
    """

    """
    zip_file = ZipFile(base_dir + '/data/santander-customer-transaction-prediction.zip')

    if which == 'all':
        dfs = {text_file.filename: pd.read_csv(zip_file.open(text_file.filename))
               for text_file in zip_file.infolist()
               if text_file.filename.endswith('.csv')}
        del dfs['sample_submission.csv']

        # change dict keys names
        dfs['train'] = dfs.pop('train.csv')
        dfs['test'] = dfs.pop('test.csv')
        return dfs

    elif which == 'test':
        dfs = {text_file.filename: pd.read_csv(zip_file.open(text_file.filename))
               for text_file in zip_file.infolist()
               if text_file.filename == 'test.csv'}
        # change dict keys names
        dfs['test'] = dfs.pop('test.csv')
        return dfs

    elif which == 'train':
        dfs = {text_file.filename: pd.read_csv(zip_file.open(text_file.filename))
               for text_file in zip_file.infolist()
               if text_file.filename == 'train.csv'}
        # change dict keys names
        dfs['train'] = dfs.pop('train.csv')
        return dfs
