"""
pre-process and save data sets for modelling.
  1 - standard scaling.
  2 - standard scaling + undersampling
  3 - standard scaling + oversampling
  4 - standard scaling + unpivot variables.
  5 - standard scaling + undersampling + unpivot variables
  6 - standard scaling + oversampling + unpivot variables
"""
if __name__ == "__main__":
    from tempfile import mkdtemp
    from shutil import rmtree
    from joblib import Memory

    import pandas as pd
    from sklearn.pipeline import Pipeline

    from src.feature_processing import feature_engineering, star_model, sampling
    from src.utils import load_datasets, save_outputs

    base_dir = '..'
    # load dataframes (returns a dictionary with both the train and test data sets,as pandas dataframe).
    dfs = load_datasets.load_base_datasets('all', base_dir)
    train_df = dfs['train']
    save_output = True

    # Create a temporary folder to store the transformers of the pipeline
    cachedir = mkdtemp()
    memory = Memory(location=cachedir, verbose=1)

    try:
        X = train_df.drop(columns=['ID_code', 'target'])
        y = train_df['target']

        numeric_column_names = list(X.columns.values)

        # just to accelerate test computation
        # X = X.drop(index=range(200, 200000))
        # y = y.drop(index=range(200, 200000))

        # pipeline to perform standard scaling of the numerical variables
        numeric_pipeline = Pipeline(
            steps=[
                ("scaler", feature_engineering.PreProcessors.standard.value)
            ]
        )

        engineered_data = feature_engineering.FeatureEngineering(numeric_column_names, [], [], False,
                                                                 numeric_transformer=numeric_pipeline)

        pipe_preproc = Pipeline([
            ('preprocessing', engineered_data.preprocessor)
        ], memory=memory)

        pipe_preproc.fit(X, y)

        # saves pipeline used for transformation
        if save_output:
            save_outputs.save_estimator(pipe_preproc, 'feature_selectors', 'preprocess_pipeline_standard')

        ####################################
        # 1 - standard scaling.
        ####################################

        # save the pre-processed df
        # train data
        df_train_preprocessed = train_df.copy()
        X_train_preprocessed = df_train_preprocessed.drop(columns=['ID_code', 'target'])
        y_train = df_train_preprocessed['target']
        X_train_preprocessed = pd.DataFrame(pipe_preproc.transform(X_train_preprocessed), columns=numeric_column_names)

        id_codes = df_train_preprocessed['ID_code']
        add_back_cols = {'ID_code': id_codes, 'target': y_train}
        df_train_preprocessed_std = X_train_preprocessed.assign(**add_back_cols)

        save_outputs.save_dataframes(X=df_train_preprocessed_std, filename='train_preprocessed_std', base_dir=base_dir, mode='parquet')

        # test data
        df_test_preprocessed = dfs['test']
        X_test_preprocessed = df_test_preprocessed.drop(columns=['ID_code'])
        X_test_preprocessed = pd.DataFrame(pipe_preproc.transform(X_test_preprocessed), columns=numeric_column_names)

        add_back_cols = {'ID_code': id_codes}
        df_test_preprocessed_std = X_test_preprocessed.assign(**add_back_cols)

        save_outputs.save_dataframes(X=df_test_preprocessed_std, filename='test_preprocessed_std', base_dir=base_dir, mode='parquet')

        ###################################
        # 2 - standard scaling + undersampling
        ####################################

        df_train_preprocessed_under = df_train_preprocessed_std.copy()
        X_preproc_to_sample = df_train_preprocessed_under.drop(columns=['ID_code', 'target'])
        y_to_sample = df_train_preprocessed_under['target']
        X_preproc_undersampled, y_preproc_undersampled = sampling.undersampler(X_preproc_to_sample, y_to_sample)

        id_codes = df_train_preprocessed_under['ID_code']
        add_back_cols = {'ID_code': id_codes, 'target': y_preproc_undersampled}
        df_train_preprocessed_under = X_preproc_undersampled.assign(**add_back_cols)

        save_outputs.save_dataframes(X=df_train_preprocessed_under, filename='train_preprocessed_std_undersampling', base_dir=base_dir, mode='parquet')

        ####################################
        # 3 - standard scaling + oversampling
        ####################################
        df_train_preprocessed_over = df_train_preprocessed_std.copy()
        X_preproc_to_sample = df_train_preprocessed_over.drop(columns=['ID_code', 'target'])
        y_to_sample = df_train_preprocessed_over['target']
        X_preproc_oversampled, y_preproc_oversampled = sampling.oversampler(X_preproc_to_sample, y_to_sample)

        id_codes = df_train_preprocessed_over['ID_code']
        add_back_cols = {'ID_code': id_codes, 'target': y_preproc_oversampled}
        df_train_preprocessed_over = X_preproc_oversampled.assign(**add_back_cols)

        save_outputs.save_dataframes(X=df_train_preprocessed_over, filename='train_preprocessed_std_oversampling', base_dir=base_dir, mode='parquet')

        ####################################
        # 4 - standard scaling + unpivot variables.
        ####################################

        # train data
        df_train_preprocessed_std_unpivot = df_train_preprocessed_std.copy()
        df_train_preprocessed_std_unpivot = star_model.melt_dataset(df=df_train_preprocessed_std_unpivot, id_vars=['ID_code', 'target'], save_output=False)
        save_outputs.save_dataframes(df_train_preprocessed_std_unpivot, 'train_preprocessed_std_unpivot', mode='parquet')

        # test data
        df_test_preprocessed_std_unpivoted = df_test_preprocessed_std.copy()
        df_test_preprocessed_std_unpivoted = star_model.melt_dataset(df=df_test_preprocessed_std_unpivoted, id_vars=['ID_code'], save_output=False)
        save_outputs.save_dataframes(df_test_preprocessed_std_unpivoted, 'test_preprocessed_std_unpivot', mode='parquet')

        ####################################
        # 5 - standard scaling + undersampling + unpivot variables
        ####################################
        # train data
        df_train_preprocessed_std_under_unpivot = df_train_preprocessed_under.copy()
        df_train_preprocessed_std_under_unpivot = star_model.melt_dataset(df=df_train_preprocessed_std_under_unpivot,
                                                                    id_vars=['ID_code', 'target'], save_output=False)
        save_outputs.save_dataframes(df_train_preprocessed_std_under_unpivot, 'train_preprocessed_std_undersampling_unpivot', mode='parquet')

        # test data
        # no test data for undersampling - no target in test.

        ####################################
        # 6 - standard scaling + oversampling + unpivot variables
        ####################################

        # train data
        df_train_preprocessed_std_over_unpivot = df_train_preprocessed_over.copy()
        df_train_preprocessed_std_over_unpivot = star_model.melt_dataset(df=df_train_preprocessed_std_over_unpivot,
                                                                          id_vars=['ID_code', 'target'],
                                                                          save_output=False)
        save_outputs.save_dataframes(df_train_preprocessed_std_over_unpivot,
                                     'train_preprocessed_std_oversampling_unpivot', mode='parquet')

        # test data
        # no test data for undersampling - no target in test.


    except Exception as e:
        print(e)
    finally:
        # delete the temporary cache before exiting
        rmtree(cachedir)

