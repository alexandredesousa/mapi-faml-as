"""
Loads pre-processed data sets - see experiment000.py

experiment: unpivot
train data: train_preprocessed_std_unpivot
test data: test_preprocessed_std_unpivot
"""
if __name__ == "__main__":
    from datetime import datetime

    import pandas as pd
    import numpy as np
    from sklearn.model_selection import train_test_split, GridSearchCV, RandomizedSearchCV
    from sklearn.utils.fixes import loguniform

    from src.modelling import modelling, evaluation
    from src.utils import load_datasets, save_outputs

    base_dir = '..'
    save_output = True

    preprocessing = 'preprocessed_std'
    sampling = None
    pivoting = 'unpivot'
    identifier = f'_{preprocessing}'
    if sampling is not None:
        identifier += f'_{sampling}'
    if pivoting is not None:
        identifier += f'_{pivoting}'
    experiment_id = f'exp003{identifier}'

    start_time = datetime.now()

    ###### modelling
    start_time_modelling = datetime.now()

    df = load_datasets.load_dataset(f'train{identifier}', base_dir=base_dir, mode='parquet')
    X = df.drop(columns=['target'])  # when unpivoting, ID_code is relevant; only drops target
    y = df['target']

    # just to accelerate test computation
    # X = X.take(np.random.randint(df.shape[0], size=200))
    # y = y.take(np.random.randint(df.shape[0], size=200))

    ## crete train and validation data sets (uses the df computed previously)
    validation_size = 0.3
    X_train, X_validate, y_train, y_validate = train_test_split(X, y, test_size=validation_size, stratify=y)

    # no transformation to perform (using transformed data set already transformed, computed in the pipe above):
    # define some classifiers to use in the ensemble

    def search_classifier(clf, grid_params, n_iter=7):
        # classifier = GridSearchCV(
        #     estimator=clf,
        #     param_grid=grid_params,
        #     scoring='roc_auc',
        #     n_jobs=-1,
        #     return_train_score=True
        # )
        classifier = RandomizedSearchCV(
            estimator=clf,
            param_distributions=grid_params,
            n_iter=n_iter,
            scoring='roc_auc',
            n_jobs=-1,
            return_train_score=True
        )
        classifier.fit(X_train, y_train)
        model_pred = classifier.predict(X_validate)
        # model_score = classifier.decision_function(X_validate)
        # model_score = classifier.predict_proba(X_validate)
        # see:
        # https://stats.stackexchange.com/questions/254710/is-it-better-to-compute-a-roc-curve-using-predicted-probabilities-or-distances-f
        # https://stackoverflow.com/questions/36543137/whats-the-difference-between-predict-proba-and-decision-function-in-scikit-lear
        if hasattr(classifier, "decision_function"):
            print('decision function')
            model_score = classifier.decision_function(X_validate)
        else:
            print('predict proba')
            model_score = classifier.predict_proba(X_validate)[:, 1]

        metrics = evaluation.get_metrics(y_validate, model_pred, model_score)
        best_results = pd.DataFrame(classifier.cv_results_)
        best_results = best_results.sort_values("mean_test_score", ascending=False)
        best_estimator = classifier.best_estimator_
        best_parameters = classifier.best_params_
        searched_space = classifier.cv_results_['params']

        return model_pred, metrics, best_results, best_estimator, best_parameters, searched_space


    # svm
    clf_svm = modelling.Classifiers_sk.svm.value
    clf_svm.set_params(probability=True)
    # svm_grid_params = {
    #     'kernel': ('linear', 'rbf'),
    #     'C': (0.01, 0.1, 1, 10)
    # }

    svm_grid_params = {'C': loguniform(1e-2, 1e3),
                       'gamma': loguniform(1e-4, 1e-3),
                       'kernel': ['linear', 'rbf']}

    svc_model_pred, svc_metrics, svc_best_results, svc_best_estimator, svc_best_params, svc_searched_params = search_classifier(clf_svm, svm_grid_params)

    print(f'SVC: {svc_metrics}')

    time_svm_end = datetime.now()
    run_time_svm = time_svm_end - start_time_modelling

    msg = (
        f"****************** GRID SEARCH ******************\n"
        f"****************** SVC"
        f"Metrics:\n{svc_metrics}\nBest params:\n{svc_best_params}\nseached params:\n{svc_searched_params}\nRun time:{run_time_svm}"
    )

    print(msg)
    if save_output:
        save_outputs.save_output_file(msg, f'{experiment_id}_results_grid', '../doc')

    # train individual best parameters in the whole training data set:
    svc_best_estimator.fit(X, y)

    df_test = load_datasets.load_dataset('test_preprocessed_std_unpivot', base_dir=base_dir, mode='parquet')
    X_test = df_test.drop(columns=['ID_code'])

    preds_best_classifier = svc_best_estimator.predict(X_test)

    # save submission
    save_outputs.prepare_submission(df_test.iloc[:, 0], preds_best_classifier,
                                    filename=f'{experiment_id}_submission_grid_best_classifier_auto', base_dir=base_dir)
    # save classifiers
    save_outputs.save_estimator(svc_best_estimator, 'classifiers', f'{experiment_id}_grid_auto_selected_best', f'{base_dir}/estimators')
