import pandas as pd

from src.utils import load_datasets, save_outputs

base_dir = '..'
df_test = load_datasets.load_base_datasets('test', base_dir=base_dir)['test']

# set proper test ID codes
ids = df_test['ID_code']

# load datasets to correct
exp007filename = 'exp007_preprocessed_std_undersampling_submission_grid_best_classifier_auto'
exp007submission = pd.read_csv(f'{base_dir}/submission/{exp007filename}.csv')
exp008filename = 'exp008_preprocessed_std_oversampling_submission_grid_best_classifier_auto'
exp008submission = pd.read_csv(f'{base_dir}/submission/{exp008filename}.csv')
exp010filename = 'exp010_preprocessed_std_submission_grid_best_classifier_auto'
exp010submission = pd.read_csv(f'{base_dir}/submission/{exp010filename}.csv')
exp011filename = 'exp011_preprocessed_std_undersampling_submission_grid_best_classifier_auto'
exp011submission = pd.read_csv(f'{base_dir}/submission/{exp011filename}.csv')

# assign correct test ids
exp007submission['ID_code'] = ids
exp008submission['ID_code'] = ids
exp010submission['ID_code'] = ids
exp011submission['ID_code'] = ids

# save predictions
exp007submission.to_csv(path_or_buf=f'{base_dir}/submission/{exp007filename}_corrected.csv', index=False)
exp008submission.to_csv(path_or_buf=f'{base_dir}/submission/{exp008filename}_corrected.csv', index=False)
exp010submission.to_csv(path_or_buf=f'{base_dir}/submission/{exp010filename}_corrected.csv', index=False)
exp011submission.to_csv(path_or_buf=f'{base_dir}/submission/{exp011filename}_corrected.csv', index=False)

print()
