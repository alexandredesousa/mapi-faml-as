from datetime import datetime

import pandas as pd
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.feature_selection import SequentialFeatureSelector

from tempfile import mkdtemp
from shutil import rmtree
from joblib import Memory

from src.modelling import modelling, evaluation
from src.utils import save_outputs


# see:
# https://scikit-learn.org/stable/modules/feature_selection.html#sequential-feature-selection
# https://scikit-learn.org/stable/modules/feature_selection.html#feature-selection-as-part-of-a-pipeline
# https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.GridSearchCV.html
# https://www.tomasbeuzen.com/post/scikit-learn-gridsearch-pipelines/
# https://stackoverflow.com/a/55629709
# https://github.com/scikit-learn/scikit-learn/issues/7536#issuecomment-251236645
# https://stackoverflow.com/a/47389468
# https://scikit-learn.org/stable/auto_examples/compose/plot_compare_reduction.html#caching-transformers-within-a-pipeline
# https://github.com/scikit-learn/scikit-learn/issues/7536#issuecomment-251236645

BASE_DIR = "../.."
def backward_elimination(X, y,
                         selection_clf,
                         selection_preprocessor,
                         n_features_select,
                         sequential_feature_selector=None,
                         save_output=False):
    start_time = datetime.now()

    # Create a temporary folder to store the transformers of the pipeline
    cachedir = mkdtemp()
    memory = Memory(location=cachedir, verbose=3)

    if sequential_feature_selector is None:
        sequential_feature_selector = SequentialFeatureSelector(selection_clf, direction='backward', n_jobs=-1)

    pipe = Pipeline([
        ('preprocessing', selection_preprocessor),
        ('feature_selection', sequential_feature_selector),
        ('classifier', selection_clf)
    ], memory=memory)

    search_space = [
        {'feature_selection__n_features_to_select': n_features_select}
    ]

    clf_select = GridSearchCV(pipe, search_space, cv=5, n_jobs=-1, verbose=4, scoring='accuracy')
    clf_select.fit(X, y)

    # Results
    best_estimator = clf_select.best_estimator_
    best_params = clf_select.best_params_
    results = clf_select.cv_results_
    pd_results = pd.DataFrame(results)
    pd_results = pd_results.sort_values("mean_test_score", ascending=False)

    selected_features_indices = best_estimator.named_steps['feature_selection'].support_
    # selected_features_names = best_estimator.named_steps['feature_selection'].get_feature_names_out() # only sklearn v > 1
    best_score = clf_select.best_score_

    run_time = datetime.now() - start_time

    msg = (
        "****************** BACKWARD ELIMINATION ******************\n"
        "Best Score (accuracy):\n{}\n"
        "Variables dict (indices):\n{}\n"
        # "Variables dict (names):\n{}\n"
        "Run time: {}"
    ).format(
        best_score,
        selected_features_indices,
        # selected_features_names,
        run_time
    )

    if save_output:
        save_outputs.save_output_file(msg, 'backward_elimination_output')

    # delete the temporary cache before exiting
    rmtree(cachedir)

    return clf_select, best_estimator, best_params, pd_results, selected_features_indices, msg

