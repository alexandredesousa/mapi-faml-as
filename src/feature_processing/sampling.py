import pandas as pd
from imblearn.over_sampling import RandomOverSampler
from imblearn.under_sampling import RandomUnderSampler
from collections import Counter # defining the dataset

from src.utils import save_outputs

base_dir = "../.."

def oversampler(X, y, save_df=False, filename=None, dir=None):
    # instantiating the random over sampler
    ros = RandomOverSampler()
    # resampling X, y
    X_ros, y_ros = ros.fit_resample(X, y)  # new class distribution

    if save_df:
        # save over sampled data
        save_outputs.save_dataframes(X=X_ros, y=y_ros, filename=filename, base_dir=dir)

    return X_ros, y_ros


def undersampler(X, y, save_df=False, filename=None, dir=None):
    # instantiating the random undersampler
    rus = RandomUnderSampler(replacement=True)
    # resampling X, y
    X_rus, y_rus = rus.fit_resample(X, y)  # new class distribution

    if save_df:
        # save under sampled data
        save_outputs.save_dataframes(X=X_rus, y=y_rus, filename=filename, base_dir=dir)

    return X_rus, y_rus
