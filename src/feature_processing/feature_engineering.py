# general
from enum import Enum
from tempfile import mkdtemp
from shutil import rmtree
from joblib import Memory
import numpy as np
import pandas as pd

# pre processing
from sklearn.pipeline import Pipeline
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import StandardScaler, MinMaxScaler, MaxAbsScaler, RobustScaler,\
    OneHotEncoder, LabelBinarizer
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import SimpleImputer, IterativeImputer, MissingIndicator, KNNImputer
# feature selection
from sklearn.feature_selection import SequentialFeatureSelector, RFECV
from sklearn.model_selection import GridSearchCV
from src.modelling import modelling


class PreProcessors(Enum):
    """
    Enum for some sklearn transformers for numerical and categorical data.

    Intended use:
    - to be integrated in a transformation pipeline.

    Currently available for numerical variables:
    standard - Standard Scaler
    minmax - Min-max scaler
    maxabsscaler - Maximum absolute value scaler

    Currently available for categorical variables:
    onehot - One-hot encoder
    lblbinarizer - Label Binarizer

    See Also
    --------
    https://scikit-learn.org/stable/modules/classes.html#module-sklearn.preprocessing
    """
    # numerical
    standard = StandardScaler()
    minmax = MinMaxScaler()
    maxabsscaler = MaxAbsScaler()
    robust = RobustScaler()

    # categorical
    onehot = OneHotEncoder(handle_unknown="ignore")
    lblbinarizer = LabelBinarizer()


class Imputers(Enum):
    """
    Enum for some sklearn transformers for data imputers.

    Intended use:
    - to be integrated in a transformation pipeline.

    Currently available:
    simple - SimpleImputer
    iterative - IterativeImputer
    missing - MissingIndicator
    neighbors - KNNImputer

    See Also
    --------
    https://scikit-learn.org/stable/modules/classes.html#module-sklearn.impute
    """
    #
    simple = SimpleImputer(strategy="median")
    iterative = IterativeImputer()
    missing = MissingIndicator()
    neighbors = KNNImputer()


def set_cols_transformer(transformer_dict):
    """
    Sets a transformer based on a dict identifying the type of the processor,
    and the lists of named steps, steps (as set in the <set_processor> routine)
    """
    transformers = [(k, set_preprocessor(v[0], v[1]), v[2]) for k, v in transformer_dict.items()]

    return ColumnTransformer(transformers=transformers, remainder='passthrough')


def set_preprocessor(named_steps, steps):
    """
    Sets a pre-processor pipeline based on a list of named_steps, and a list of the steps.
    The steps must be available in the PreProcessors enum.

    Notes:
        Cannot use the Imputers.

    Parameters
    ----------
    named_steps : list
        The intended named steps (named steps in the Pipeline parameter).
    steps : list
        The intended steps.
        Should contain strings that match the keys of the PreProcessor enum.
    """
    steps = [(ns, PreProcessors[s].value) for ns, s in zip(named_steps, steps)]

    return Pipeline(steps=steps)


class FeatureEngineering:
    """
    Encapsulates the transformations to be performed in a ColumnTransformer
    and provides an utility fit function.
    Provides basic routines to set a default numeric transformer, and a default categorical transformer.

    Notes:
    If a category of features does not exist (for instance, ordinal), an empty list should be passed.

    Parameters
    ----------
    numeric_features : list
        List with the names of the features to be treated as numeric
        (i.e. to which the numeric_transformer is to be applied)

    categorical_features : list
        List with the names of the features to be treated as categorical
        (i.e. to which the categorical_transformer is to be applied)

    ordinal_features : list
        List with the names of the features to be treated as ordinal
        (i.e. to which the ordinal_transformer is to be applied)

    ordinal_as_cat : boolean
        To be deprecated!

    numeric_transformer : ``Pipeline`` instance, optional
        Pipeline with the numeric transformations to be applied to numeric_features.
        If none is provided, the method self.default_numeric_transformer() is used to create a default one.

    categorical_transformer : ``Pipeline`` instance, optional
        Pipeline with the numeric transformations to be applied to categorical_features.
        If none is provided, the method self.default_categorical_transformer() is used to create a default one.

    ordinal_transformer : ``Pipeline`` instance, optional
        Pipeline with the numeric transformations to be applied to numeric_features.
        If none is provided, the method self.default_numeric_transformer() is used to create a default one.
    """
    def __init__(self,
                 numeric_features, categorical_features, ordinal_features, ordinal_as_cat,
                 numeric_transformer=None, categorical_transformer=None, ordinal_transformer=None):
        if ordinal_as_cat:
            self.categorical_features = categorical_features + ordinal_features
            self.numeric_features = numeric_features
        else:
            self.categorical_features = categorical_features
            self.numeric_features = numeric_features + ordinal_features

        # numeric variables transformer
        if numeric_transformer is None:
            self.numeric_transformer = self.default_numeric_transformer()
        else:
            self.numeric_transformer = numeric_transformer

        # categorical variables transformer
        if categorical_transformer is None:
            self.categorical_transformer = self.default_categorical_transformer()
        else:
            self.categorical_transformer = categorical_transformer

        # should add an ordinal variables transformer?
        # for the case where the user does not want ordinals neither handled as numeric nor categorical.

        self.preprocessor = ColumnTransformer(
            transformers=[
                ('num', self.numeric_transformer, self.numeric_features),
                ('cat', self.categorical_transformer, self.categorical_features)
            ],
            remainder='passthrough'
        )

        # only training data is used to fit the pre-processing transformer (no data leakage)
        # self.preprocessor.fit(self.train_data)

        # after the fitting process the variable names are lost. recover:
        # self.fit_preproc_categories = self.preprocessor.named_transformers_['cat'].named_steps[
        #    'onehot'].get_feature_names()
        # self.fit_preproc_vars = np.append(self.num_vars, self.fit_preproc_categories)

    @staticmethod
    def default_numeric_transformer():
        """
        Create a Pipeline to be used as numeric transformer.

        Intended for use when none is provided, so this is used as defualt.

        Returns
        -------
        Pipeline with an NA simple imputer, and a standard scaler.
        """
        numeric_pipeline = Pipeline(
            steps=[
                ("imputer", Imputers.simple.value),
                ("scaler", PreProcessors.robust.value)
            ]
        )
        return numeric_pipeline

    @staticmethod
    def default_categorical_transformer():
        """
        Create a Pipeline to be used as categorical transformer.

        Intended for use when none is provided, so this is used as defualt.

        Returns
        -------
        Pipeline with an one-hot encoder.
        """
        categorical_pipeline = Pipeline(
            steps=[
                ('onehot', PreProcessors.onehot.value)
            ]
        )
        return categorical_pipeline

    def fit_test(self, X, y=None):
        """
        utility function
        """
        self.X_encoded = pd.DataFrame(self.preprocessor.fit_transform(X, y))


class FeaturesSelection:
    def __init__(self, n_features_to_select, scoring='roc_auc',
                 selector='sequential', direction='backward',
                 cv=5, n_jobs=-1,
                 estimator=None):
        """
        Parameters
        ----------
        n_features_to_select : int or float
            The number of features to be selected in the selection process.
            Corresponds to the parameter n_features_to_select of the SequentialFeatureSelector,
            or the parameter min_features_to_select of the RFECV.
            For the case when selector is a SequentialFeatureSelector, can be an int or float,
            where the int is the absolute number of features to be selected, whereas if float
            it is the proportion of features to be selected, should be between 0 and 1.
            For the case when the selector is an RFECV, it has to be an int, with the absolute number
            of features to be selected.

        scoring : str, optional
            A string specifying the scoring to evaluate the predictions on the test set.
            See sklearn predefined values for available options (link in See Also section).
            Defaults to 'roc_auc'.

        cv : int, optional
            Splitting strategy for cross-validation. Defaults to 5.

        n_jobs : int, optional
            Number of jobs to run in parallel.
            A value of -1 stands for using all processors.
            Defaults to -1.

        direction : str, optional
            Should take values 'forward' (standing for forward selection)
            or 'backward' (standing for backward selection).
            Only applicable if selector == 'sequential'.

        selector : str, optional
            The selector to be used - either 'sequential' to use a SequentialFeatureSelector() instance,
            or 'recursive' to use an RFECV() instance.
            Defaults to 'sequential'.

        estimator : ``Estimator`` instance, optional
            An estimator to be used in the selection process.
            For the case where selector == 'sequential', it should be an unfitted estimator,
            whereas for selector == 'recursive' it should be a fitted estimator.
            See documentation of each method for details.

        See Also
        --------
            https://scikit-learn.org/0.24/modules/generated/sklearn.feature_selection.SequentialFeatureSelector.html?highlight=sequential%20feature%20selector#sklearn.feature_selection.SequentialFeatureSelector
            https://scikit-learn.org/0.24/modules/generated/sklearn.feature_selection.RFECV.html?highlight=rfecv#sklearn.feature_selection.RFECV
            https://scikit-learn.org/0.24/modules/model_evaluation.html#scoring-parameter

        """
        if estimator is None:
            self.estimator = modelling.Classifiers_sk.svm.value
        else:
            self.estimator = estimator

        if selector == 'sequential':
            self.selector = SequentialFeatureSelector(estimator=self.estimator, n_features_to_select=n_features_to_select,
                                                      direction=direction, scoring=scoring, cv=cv, n_jobs=n_jobs)
        elif selector == 'recursive':
            self.selector = RFECV(estimator=estimator, min_features_to_select=n_features_to_select,
                                  cv=cv, scoring=scoring, n_jobs=n_jobs)

        self.grid_selector = None

    def fit(self, features, target, preprocessor, search_space_selection=None):

        if search_space_selection is None:
            search_space_selection = {}

        # Create a temporary folder to store the transformers of the pipeline
        cachedir = mkdtemp()
        memory = Memory(location=cachedir, verbose=1)

        try:
            selection_pipeline = Pipeline([
                ('preprocessing', preprocessor),
                ('feature_selection', self.selector),
                ('classifier', self.estimator)
            ], memory=memory)

            self.grid_selector = GridSearchCV(
                selection_pipeline, search_space_selection, cv=3, n_jobs=-1, verbose=1, scoring='roc_auc'
            )

            self.grid_selector.fit(features, target)

        except Exception as e:
            print(e)
        finally:
            # delete the temporary cache before exiting32
            rmtree(cachedir)

        # self.selector.fit(features, target)

class DataTransformer:
    """

    """
    def __init__(self, steps=None, cachedir_memory=None, verbose=None):

        self.steps = [('init', 'passthrough')] if steps is None else steps

        # Create a temporary folder to store the transformers of the pipeline
        self.cachedir = mkdtemp() if cachedir_memory is None else cachedir_memory
        self.memory = Memory(location=self.cachedir, verbose=1)

        verbose = 1 if verbose is None else verbose

        self.transform_pipeline = Pipeline(steps=self.steps, memory=self.memory, verbose=verbose)
        # The initial step is dropped - it was only required to initialize the Pipeline.
        self.transform_pipeline.steps.pop(0)

        # To be set on the fit method.
        self.grid_selector = None

    def add_steps(self, steps):
        """
        steps must be a Pipeline, ColumnTransformer
        """
        self.transform_pipeline.steps.append(steps)

    def fit(self, X, y, search_space_selection=None):
        if search_space_selection is None:
            search_space_selection = {}

        try:
            self.grid_selector = GridSearchCV(self.transform_pipeline, search_space_selection,
                                              cv=3, n_jobs=-1, verbose=1, scoring='roc_auc')
            self.grid_selector.fit(X, y)

        except Exception as e:
            print('Failed fitting DataTransformer with: {}'.format(e))
        finally:
            # delete the temporary cache before exiting32
            rmtree(self.cachedir)
