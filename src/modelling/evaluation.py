from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, roc_auc_score, confusion_matrix

def get_metrics(y_true, y_pred, y_score=None):
    """
    Computes evaluation metrics.

    Parameters
    ----------

    Returns
    -------

    See Also
    --------
    https://scikit-learn.org/stable/auto_examples/miscellaneous/plot_roc_curve_visualization_api.html
    https://scikit-learn.org/stable/auto_examples/miscellaneous/plot_roc_curve_visualization_api.html

    Examples
    --------

    """
    accuracy = accuracy_score(y_true, y_pred)
    precision = precision_score(y_true, y_pred)
    recall = recall_score(y_true, y_pred)
    f1s = f1_score(y_true, y_pred)
    roc_auc = roc_auc_score(y_true, y_score)

    return accuracy, precision, recall, f1s, roc_auc