# general
from datetime import datetime
from enum import Enum

# pre processing
from sklearn.pipeline import Pipeline

# classifiers
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, GradientBoostingClassifier

# hyper parameter tuning
from sklearn.model_selection import GridSearchCV

from src.modelling import evaluation


class Classifiers_sk(Enum):
    """
    Enum for some sklearn classifiers as value.

    Returns a classifer with the default parameters.

    Intended use:
    - rapid prototyping,
    - integration in a pipeline performing hyper-parameter tuning, where initial parameters are not critical.

    Currently available:
    svm - Support Vector Machine classifier
    dt - Decision Tree Classifier
    lr - Logistic Regression Classifier
    knn - K-Nearest Neighbors Classifier
    nb - Naive Bayes Classifier
    mlp - Multi-layer Perceptron classifier
    rf - Random Forest Classifier
    ada - Ada Boost Classifier
    gbt - Gradient Tree Boosting

    See Also
    --------
    https://scikit-learn.org/stable/auto_examples/classification/plot_classifier_comparison.html
    """

    svm = SVC()
    dt = DecisionTreeClassifier()
    lr = LogisticRegression()
    knn = KNeighborsClassifier()
    nb = GaussianNB()
    mlp = MLPClassifier()
    rf = RandomForestClassifier()
    ada = AdaBoostClassifier()
    gbt = GradientBoostingClassifier()


class Classifiers:
    """
    Set sklearn classifiers either using default values or parameterized.
    Utility class.

    Parameters
    ----------
    clf_str : str
        classifier identifier as ID.
        Should be available one of the members of the Classifiers_sk enumeration
        (i.e. should be in Classifiers_sk.__members__.keys()).

    clf_params : dict, optional
        Dictionary with parameters: values for the classifier.

    Returns
    -------
    clf :
        An sklearn classifier.

    See Also
    --------

    Examples
    --------

    """

    CLASSIFIERS_SK = Classifiers_sk.__members__.keys()

    def __init__(self, clf_str, **kwargs):
        """
        **kwargs must specify the parameters for the classifier identified in clf_str,
        and these may differ depending on the classifier.
        """
        self.clf_str = clf_str
        self.clf_params = kwargs #['clf_params'] if 'clf_params' in kwargs.keys() else None
        self.clf = self.set_classifier(self.clf_str, self.clf_params)

    @staticmethod
    def get_classifier_sk_default(clf_str):
        """
        Returns an sklearn classifier with the default parameters.
        """
        for name, member in Classifiers_sk.__members__.items():
            if name == clf_str:
                return member.value

    def set_classifier(self, clf_str, clf_params):
        """
        Sets a classifier - either with default arguments or differently parameterized,
        depending if clf_params is not empty. Always sets as default. In case there are arguments,
        sets them after getting the model using set_params method.
        """

        if clf_str in Classifiers.CLASSIFIERS_SK:
            clf = self.get_classifier_sk_default(clf_str)

            if bool(clf_params):  # params dictionary is not empty
                clf.set_params(**clf_params)

            return clf


class ModelIntegrated:
    """
    Class to encapsulate the modelling data.

    Holds pre-processing, classifier parameters, and the classifier itself.
    Regarding data treatment, it should be an object DataTransformer.

    Parameters
    ----------
    classifier_str : str, optional
        A string specifying which estimator instance is to be created as classifier.
        Should be available one of the members of the Classifiers_sk enumeration
        (i.e. should be in Classifiers_sk.__members__.keys()), i.e.
        {'svm', 'dt', 'lr', 'knn', 'nb', 'mlp', 'e_rf', 'e_ada', 'e_gtb'}.

    data_transformer : ``DataTransformer`` instance, optional
        DataTransformer that should contain the required steps for transforming the data.
        For details on the object see feature_engineering.DataTransformer class.

        This parameter is optional, to allow the user to load a pre-processed data base,
        in which case, there is no need to compute the pre-processing to save computing power,
        then no need for the data transformer.

    model_params : dict, optional
        A dict containing the parameters for the estimator instance.
        The dictionary should be constructed such that key:value stand for parameter:argument, for the required estimator.
        Values depending on the estimator required.
        Defaults to None, in which case sklearn default arguments for the estimator are used
        otherwise uses the arguments passed.

    """
    def __init__(self,
                 classifier_str,
                 model_params=None,
                 data_transformer=None,):
        self.clf_str = classifier_str
        self.data_transformer = data_transformer
        self.model_params = model_params
        # self.classifier = Classifiers(self.clf_str) if self.model_params is None \
        #     else Classifiers(self.clf_str, **self.model_params)
        # self.clf = self.classifier.clf
        self.classifier = Classifiers(self.clf_str).clf if self.model_params is None \
            else Classifiers(self.clf_str, **self.model_params).clf

        self.model_pipeline = self.classifier if data_transformer is None else data_transformer.add_steps([('classifier', self.classifier)])

        # to be set by fit routines
        self.metrics = {}
        self.tuner = None

        self.times = {'modelling instantiation time': datetime.now()}

    def fit(self, X, y, grid_params=None):
        fit_init_time = datetime.now()
        grid_params = {} if grid_params is None else grid_params
        self.tuner = GridSearchCV(self.model_pipeline, grid_params, cv=5, verbose=0, n_jobs=-1)
        self.tuner.fit(X, y)


        times_id = 'grid' if grid_params is not None else ''
        self.times['modelling {} fit run time'.format(times_id)] = datetime.now() - fit_init_time

        return self.tuner.best_params_, self.tuner.best_estimator_

    def evaluate(self, model_type, X_val, y_val):
        preds = self.tuner.predict(X_val)
        # get metrics with roc_auc if supported. Otherwise, do not compute roc_auc
        if hasattr(self.tuner, 'predict_proba'):
            self.metrics[model_type] = evaluation.get_metrics(y_val, preds, self.tuner.predict_proba(X_val)[:, 1])
        elif hasattr(self.tuner, 'decision_function'):
            self.metrics[model_type] = evaluation.get_metrics(y_val, preds, self.tuner.decision_function(X_val))
        else:
            self.metrics[model_type] = evaluation.get_metrics(y_val, preds)

class Model:
    """
    # TODO: Depecate this class in favor of ModelIntegrated (which should then be called Model)
    Class to encapsulate the modelling data.

    Holds pre-processing, classifier parameters, and the classifier itself.

    Parameters
    ----------
    classifier_str : str, optional
        A string specifying which estimator instance is to be created as classifier.
        Should be available one of the members of the Classifiers_sk enumeration
        (i.e. should be in Classifiers_sk.__members__.keys()), i.e.
        {'svm', 'dt', 'lr', 'knn', 'nb', 'mlp', 'e_rf', 'e_ada', 'e_gtb'}.

    pre_processing : ```Transformer``instance
        ColumnTrnasformer that should contain the required steps for pre-processing.
        Recommended to use FeatureSelection.preprocessor, though not required.

    model_params : dict, optional
        A dict containing the parameters for the estimator instance.
        The dictionary should be constructed such that key:value stand for parameter:argument, for the required estimator.
        Values depending on the estimator required.
        Defaults to None, in which case sklearn default arguments for the estimator are used
        otherwise uses the arguments passed.

    """
    def __init__(self,
                 classifier_str,
                 pre_processing,
                 model_params=None):
        self.clf_str = classifier_str
        self.pre_processing = pre_processing
        self.model_params = model_params
        self.classifier = Classifiers(self.clf_str) if self.model_params is None \
            else Classifiers(self.clf_str, **self.model_params)
        self.clf = self.classifier.clf

        self.full_pipeline = Pipeline(
            steps=[
                ('preprocessor', self.pre_processing),
                ('classifier', self.clf)
            ]
        )

    def fit(self, features, target):
        self.full_pipeline.fit(features, target)

    def grid_search(self, features, target, grid_params=None):

        self.grid_params = grid_params

        # settings
        grid = GridSearchCV(self.clf, self.grid_params, cv=5, verbose=0, n_jobs=-1)

        # model train
        self.fit_clf = grid.fit(features, target)
        self.best_params = grid.best_params_
        self.best_estimator = grid.best_estimator_

        return self.best_params, self.best_estimator
