from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, LSTM, Activation, Dropout
from tensorflow.keras import optimizers, metrics

## function to setup model - assuming multiclass classification problem
def setup_model(topo, dropout_rate, input_size, output_size):
    model = Sequential()
    model.add(Dense(topo[0], activation="relu", input_dim=input_size))
    # model.add(LSTM(topo[0], activation="relu", input_dim=input_size, return_sequences = True))
    if dropout_rate > 0:
        model.add(Dropout(dropout_rate))
    for i in range(1, len(topo)):
        model.add(Dense(topo[i], activation="relu"))
        # model.add(LSTM(topo[i], activation="relu", return_sequences=True))
        if dropout_rate > 0:
            model.add(Dropout(dropout_rate))
    model.add(Dense(output_size))
    model.add(Activation('softmax'))

    return model


## training the DNN - takes algorithm (string) and learning rate; data (X, y), epochs and batch size
def train_dnn(model, alg, lr, Xtrain, Ytrain, epochs=5, batch_size=64):
    if alg == "adam":
        optimizer = optimizers.Adam(lr=lr)
    elif alg == "rmsprop":
        optimizer = optimizers.RMSprop(lr=lr)
    elif alg == "sgd_momentum":
        optimizer = optimizers.SGD(lr=lr, momentum=0.9)
    else:
        optimizer = optimizers.SGD(lr=lr)

    model.compile(optimizer=optimizer,
                  loss="categorical_crossentropy",
                  metrics=[metrics.AUC()]
                  )
    model.fit(Xtrain, Ytrain, epochs=epochs, batch_size=batch_size, verbose=0)

    return model


## optimizing parameters: topology, algorithm, learning rate, dropout
## randomized search optimization with maximum iterations
## takes as input: dictionary with params to optimizae and possible values; training data(X,y), validation data (X,y), iterations, epochs for training
def dnn_optimization(opt_params, Xtrain, Ytrain, Xval, Yval, iterations=10, epochs=5, verbose=True):
    from random import choice

    if verbose:
        print("Topology\tDropout\tAlgorithm\tLRate\tValLoss\tValAUC\n")
    best_auc = None

    input_size = Xtrain.shape[1]
    output_size = Ytrain.shape[1]

    if "topology" in opt_params:
        topologies = opt_params["topology"]
    else:
        topologies = [[100]]
    if "algorithm" in opt_params:
        algs = opt_params["algorithm"]
    else:
        algs = ["adam"]
    if "lr" in opt_params:
        lrs = opt_params["lr"]
    else:
        lrs = [0.001]
    if "dropout" in opt_params:
        dropouts = opt_params["dropout"]
    else:
        dropouts = [0.0]

    for it in range(iterations):
        topo = choice(topologies)
        dropout_rate = choice(dropouts)
        dnn = setup_model(topo, dropout_rate, input_size, output_size)
        alg = choice(algs)
        lr = choice(lrs)
        dnn = train_dnn(dnn, alg, lr, Xtrain, Ytrain, epochs, 128)
        val_loss, val_auc = dnn.evaluate(Xval, Yval, verbose=0)

        if verbose:
            print(topo, "\t", dropout_rate, "\t", alg, "\t", lr, "\t", val_loss, "\t", val_auc)

        if best_auc is None or val_auc > best_auc:
            best_auc = val_auc
            best_config = (topo, dropout_rate, alg, lr)

    return best_config, best_auc