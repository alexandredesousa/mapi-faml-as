from enum import Enum

# scikit-learn classifiers
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, GradientBoostingClassifier

# tensorflow/keras
from tensorflow.keras import models
from tensorflow.keras.layers import Dense, LSTM


class Classifiers_tf(Enum):
    dense = models.Sequential(Dense())
    lstm = models.Sequential(LSTM())

class Classifiers_sk(Enum):
    """
    Enum for some sklearn classifiers as value.

    Returns a classifer with the default parameters.

    Intended use:
    - rapid prototyping,
    - integration in a pipeline performing hyper-parameter tuning, where initial parameters are not critical.

    Currently available:
    svm - Support Vector Machine classifier
    dt - Decision Tree Classifier
    lr - Logistic Regression Classifier
    knn - K-Nearest Neighbors Classifier
    nb - Naive Bayes Classifier
    mlp - Multi-layer Perceptron classifier
    rf - Random Forest Classifier
    ada - Ada Boost Classifier
    gbt - Gradient Tree Boosting

    See Also
    --------
    https://scikit-learn.org/stable/auto_examples/classification/plot_classifier_comparison.html
    """

    svm = SVC()
    dt = DecisionTreeClassifier()
    lr = LogisticRegression()
    knn = KNeighborsClassifier()
    nb = GaussianNB()
    mlp = MLPClassifier()
    rf = RandomForestClassifier()
    ada = AdaBoostClassifier()
    gbt = GradientBoostingClassifier()


class Classifiers:
    """
    Set sklearn classifiers either using default values or parameterized.
    Utility class.

    Parameters
    ----------
    clf_str : str
        classifier identifier as ID.
        Should be available one of the members of the Classifiers_sk enumeration
        (i.e. should be in Classifiers_sk.__members__.keys()).

    clf_params : dict, optional
        Dictionary with parameters: values for the classifier.

    Returns
    -------
    clf :
        An sklearn classifier.

    See Also
    --------

    Examples
    --------

    """

    CLASSIFIERS_SK = Classifiers_sk.__members__.keys()

    def __init__(self, clf_str, **kwargs):
        """
        **kwargs must specify the parameters for the classifier identified in clf_str,
        and these may differ depending on the classifier.
        """
        self.clf_str = clf_str
        self.clf_params = kwargs #['clf_params'] if 'clf_params' in kwargs.keys() else None
        self.clf = self.set_classifier(self.clf_str, self.clf_params)

    @staticmethod
    def get_classifier_sk_default(clf_str):
        """
        Returns an sklearn classifier with the default parameters.
        """
        for name, member in Classifiers_sk.__members__.items():
            if name == clf_str:
                return member.value

    def set_classifier(self, clf_str, clf_params):
        """
        Sets a classifier - either with default arguments or differently parameterized,
        depending if clf_params is not empty. Always sets as default. In case there are arguments,
        sets them after getting the model using set_params method.
        """

        if clf_str in Classifiers.CLASSIFIERS_SK:
            clf = self.get_classifier_sk_default(clf_str)

            if bool(clf_params):  # params dictionary is not empty
                clf.set_params(**clf_params)

            return clf