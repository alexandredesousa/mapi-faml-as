"""
Replicates experiment002 using gradientboosting

Loads pre-processed data sets - see experiment000.py

experiment: oversampling
train data: train_preprocessed_std_oversampling
test data: test_preprocessed_std
"""
if __name__ == "__main__":
    from datetime import datetime

    import pandas as pd
    import numpy as np
    from sklearn.model_selection import train_test_split, GridSearchCV, RandomizedSearchCV
    from sklearn.utils.fixes import loguniform
    from scipy.stats import uniform as sp_randFloat
    from scipy.stats import randint as sp_randInt

    from src.modelling import modelling, evaluation
    from src.utils import load_datasets, save_outputs

    base_dir = '..'
    save_output = True

    preprocessing = 'preprocessed_std'
    sampling = 'oversampling'
    pivoting = None
    identifier = f'_{preprocessing}'
    if sampling is not None:
        identifier += f'_{sampling}'
    if pivoting is not None:
        identifier += f'_{pivoting}'
    experiment_id = f'exp008{identifier}'

    start_time = datetime.now()

    ###### modelling
    start_time_modelling = datetime.now()

    df = load_datasets.load_dataset(f'train{identifier}', base_dir=base_dir, mode='parquet')
    X = df.drop(columns=['ID_code', 'target'])
    y = df['target']

    # just to accelerate test computation
    # X = X.take(np.random.randint(df.shape[0], size=200))
    # y = y.take(np.random.randint(df.shape[0], size=200))

    ## crete train and validation data sets (uses the df computed previously)
    validation_size = 0.3
    X_train, X_validate, y_train, y_validate = train_test_split(X, y, test_size=validation_size)

    # no transformation to perform (using transformed data set already transformed, computed in the pipe above):
    # define some classifiers to use in the ensemble

    def search_classifier(clf, grid_params, n_iter=7):
        # classifier = GridSearchCV(
        #     estimator=clf,
        #     param_grid=grid_params,
        #     scoring='roc_auc',
        #     n_jobs=-1,
        #     return_train_score=True
        # )
        classifier = RandomizedSearchCV(
            estimator=clf,
            param_distributions=grid_params,
            n_iter=n_iter,
            scoring='roc_auc',
            n_jobs=-1,
            return_train_score=True
        )
        classifier.fit(X_train, y_train)
        model_pred = classifier.predict(X_validate)
        # model_score = classifier.decision_function(X_validate)
        # model_score = classifier.predict_proba(X_validate)
        # see:
        # https://stats.stackexchange.com/questions/254710/is-it-better-to-compute-a-roc-curve-using-predicted-probabilities-or-distances-f
        # https://stackoverflow.com/questions/36543137/whats-the-difference-between-predict-proba-and-decision-function-in-scikit-lear
        if hasattr(classifier, "decision_function"):
            model_score = classifier.decision_function(X_validate)
        else:
            model_score = classifier.predict_proba(X_validate)[:, 1]

        metrics = evaluation.get_metrics(y_validate, model_pred, model_score)
        best_results = pd.DataFrame(classifier.cv_results_)
        best_results = best_results.sort_values("mean_test_score", ascending=False)
        best_estimator = classifier.best_estimator_
        best_parameters = classifier.best_params_
        searched_space = classifier.cv_results_['params']

        return model_pred, metrics, best_results, best_estimator, best_parameters, searched_space


    # gbt
    clf_gbt = modelling.Classifiers_sk.gbt.value
    clf_gbt.set_params(verbose=2, n_iter_no_change=5, tol=0.01)
    # svm_grid_params = {
    #     'kernel': ('linear', 'rbf'),
    #     'C': (0.01, 0.1, 1, 10)
    # }

    gbt_grid_params = {
        'n_estimators': sp_randInt(50, 500),
        'learning_rate': sp_randFloat(),
        'min_samples_split': sp_randInt(2, 2000),
        'max_depth': sp_randInt(3, 10)
    }

    gbt_model_pred, gbt_metrics, gbt_best_results, gbt_best_estimator, gbt_best_params, gbt_searched_params = search_classifier(clf_gbt, gbt_grid_params)

    print(f'GBT: {gbt_metrics}')

    time_gbt_end = datetime.now()
    run_time_gbt = time_gbt_end - start_time_modelling

    msg = (
        f"****************** GRID SEARCH ******************\n"
        f"****************** GBT"
        f"Metrics:\n{gbt_metrics}\nBest params:\n{gbt_best_params}\nseached params:\n{gbt_searched_params}\nRun time:{run_time_gbt}"
    )

    print(msg)
    if save_output:
        save_outputs.save_output_file(msg, f'{experiment_id}_results_grid', '../doc')

    # train individual best parameters in the whole training data set:
    gbt_best_estimator.fit(X, y)

    df_test = load_datasets.load_dataset('test_preprocessed_std', base_dir=base_dir, mode='parquet')
    X_test = df_test.drop(columns=['ID_code'])

    preds_best_classifier = gbt_best_estimator.predict(X_test)

    # save submission
    save_outputs.prepare_submission(df_test.iloc[:, 0], preds_best_classifier,
                                    filename=f'{experiment_id}_submission_grid_best_classifier_auto', base_dir=base_dir)
    # save classifiers
    save_outputs.save_estimator(gbt_best_estimator, 'classifiers', f'{experiment_id}_grid_auto_selected_best', f'{base_dir}/estimators')
