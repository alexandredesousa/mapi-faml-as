"""
See Also
--------
https://stackoverflow.com/questions/419163/what-does-if-name-main-do
"""
if __name__ == "__main__":
    from datetime import datetime
    from tempfile import mkdtemp
    from shutil import rmtree
    from joblib import Memory

    import pandas as pd
    from sklearn.pipeline import Pipeline
    from sklearn.model_selection import train_test_split, GridSearchCV, RandomizedSearchCV
    from sklearn.ensemble import VotingClassifier
    from sklearn.utils.fixes import loguniform

    from src.feature_processing import feature_engineering
    from src.modelling import modelling, evaluation
    from src.utils import load_datasets, save_outputs

    start_time = datetime.now()

    base_dir = '../..'
    # load dataframes (returns a dictionary with both the train and test data sets,as pandas dataframe).
    dfs = load_datasets.load_base_datasets('all', '../..')
    train_df = dfs['train']
    save_output = False

    # Create a temporary folder to store the transformers of the pipeline
    cachedir = mkdtemp()
    memory = Memory(location=cachedir, verbose=1)

    try:
        X = train_df.drop(columns=['ID_code', 'target'])
        y = train_df['target']

        # just to accelerate test computation
        X = X.drop(index=range(200, 200000))
        y = y.drop(index=range(200, 200000))

        numeric_pipeline = Pipeline(
            steps=[
                ("scaler", feature_engineering.PreProcessors.standard.value)
            ]
        )

        engineered_data = feature_engineering.FeatureEngineering(list(X.columns.values), [], [], False,
                                                                 numeric_transformer=numeric_pipeline)

        pipe_preproc = Pipeline([
            ('preprocessing', engineered_data.preprocessor)
        ], memory=memory)

        pipe_preproc.fit(X, y)

        if save_output:
            save_outputs.save_estimator(pipe_preproc, 'feature_selectors', 'preprocess_pipeline_standard')

        # save the pre-processed df
        # train data
        df_train_preprocessed = train_df.copy()
        X_train_preprocessed = df_train_preprocessed.drop(columns=['ID_code', 'target'])
        y_train = df_train_preprocessed['target']
        #X_selected = pd.DataFrame(pipe_preproc.transform(X_train_preprocessed).toarray())
        X_selected = pd.DataFrame(pipe_preproc.transform(X_train_preprocessed))
        save_outputs.save_dataframes(X=X_selected, y=y_train, filename='train_preprocessed_std', base_dir=base_dir)

        # test data
        # df_test = pd.read_csv(base_dir + '/data/test.csv')
        df_test_preprocessed = dfs['test']
        X_test_preprocessed = df_test_preprocessed.drop(columns=['ID_code'])
        # X_selected_test = pd.DataFrame(pipe_preproc.transform(X_test_preprocessed).toarray())
        X_test_preprocessed = pd.DataFrame(pipe_preproc.transform(X_test_preprocessed))
        save_outputs.save_dataframes(X=X_test_preprocessed, filename='test_preprocessed_std', base_dir=base_dir)
    except Exception as e:
        print(e)
    finally:
        # delete the temporary cache before exiting
        rmtree(cachedir)

    ###### modelling
    start_time_modelling = datetime.now()

    ## crete train and validation data sets (uses the df computed above)
    # df = pd.read_csv(base_dir + '/data/train_preprocessed_feature_drop_auto.csv')
    X = X_train_preprocessed.drop(index=range(200, 200000))
    y = y_train.drop(index=range(200, 200000))

    validation_size = 0.3
    X_train, X_validate, y_train, y_validate = train_test_split(X, y, test_size=validation_size, stratify=y)

    # no transformation to perform (using transformed data set already transformed, computed in the pipe above):
    # define some classifiers to use in the ensemble

    def search_classifier(clf, grid_params):
        classifier = GridSearchCV(
            estimator=clf,
            param_grid=grid_params,
            scoring='roc_auc',
            n_jobs=-1,
            return_train_score=True
        )
        classifier.fit(X_train, y_train)
        model_pred = classifier.predict(X_validate)
        # model_score = classifier.decision_function(X_validate)
        # model_score = classifier.predict_proba(X_validate)
        # see:
        # https://stats.stackexchange.com/questions/254710/is-it-better-to-compute-a-roc-curve-using-predicted-probabilities-or-distances-f
        # https://stackoverflow.com/questions/36543137/whats-the-difference-between-predict-proba-and-decision-function-in-scikit-lear
        if hasattr(classifier, "decision_function"):
            print('decision function')
            model_score = classifier.decision_function(X_validate)
        else:
            print('predict proba')
            model_score = classifier.predict_proba(X_validate)[:, 1]

        metrics = evaluation.get_metrics(y_validate, model_pred, model_score)
        best_results = pd.DataFrame(classifier.cv_results_)
        best_results = best_results.sort_values("mean_test_score", ascending=False)
        best_estimator = classifier.best_estimator_
        best_parameters = classifier.best_params_

        return model_pred, metrics, best_results, best_estimator, best_parameters


    # svm
    clf_svm = modelling.Classifiers_sk.svm.value
    clf_svm.set_params(probability=True)
    svm_grid_params = {
        'kernel': ('linear', 'rbf'),
        'C': (0.01, 0.1, 1, 10)
    }
    svc_model_pred, svc_metrics, svc_best_results, svc_best_estimator, svc_best_params = search_classifier(clf_svm,
                                                                                                           svm_grid_params)

    print('SVC: {}'.format(svc_metrics))

    time_svm_end = datetime.now()
    run_time_svm = time_svm_end - start_time_modelling

    # gbt
    clf_gbt = modelling.Classifiers_sk.gbt.value
    gbt_grid_params = {
        'n_estimators': (50, 100, 200, 500),
        'learning_rate': (0.01, 0.05, 0.1, 0.2, 0.3, 0.7),
        'min_samples_split': (2, 10, 50, 100, 500, 1000, 2000),
        'max_depth': (3, 5, 10)
    }
    gbt_model_pred, gbt_metrics, gbt_best_results, gbt_best_estimator, gbt_best_params = search_classifier(clf_gbt,
                                                                                                           gbt_grid_params)

    time_gbt_end = datetime.now()
    run_time_gbt = time_gbt_end - time_svm_end

    eclf = VotingClassifier(
        estimators=[
            ('svm', clf_svm),
            ('gbt', clf_gbt)
        ],
        # voting='hard'
        voting='soft')

    ensemble_grid_params = {
        'svm__kernel': ('linear', 'rbf'),
        'svm__C': (0.01, 0.1, 1, 10),
        'gbt__n_estimators': (50, 100, 200, 500),
        'gbt__learning_rate': (0.01, 0.05, 0.1, 0.2, 0.3, 0.7),
        'gbt__min_samples_split': (2, 10, 50, 100, 500, 1000, 2000),
        'gbt__max_depth': (3, 5, 10)
    }

    eclf_model_pred, eclf_metrics, eclf_best_results, eclf_best_estimator, eclf_best_params = search_classifier(eclf,
                                                                                                              ensemble_grid_params)

    print('Ensemble: {}'.format(eclf_metrics))

    time_ensemble_end = datetime.now()
    run_time_ensemble = time_ensemble_end - time_gbt_end

    # set best classifier (considering accuracy):
    best_classifiers = [svc_best_estimator, eclf_best_estimator]
    best_accuracies = [svc_metrics[4], eclf_metrics[4]]
    max_score_index = best_accuracies.index(max(best_accuracies))
    best_score = best_accuracies[max_score_index]
    best_classifier = best_classifiers[max_score_index]

    msg = (
        "****************** GRID SEARCH ******************\n"
        "****************** SVC"
        "Metrics:\n{}\nBest params:\n{}\nRun time:{}"
        "****************** GBT"
        "Metrics:\n{}\nBest params:\n{}\nRun time:{}"
        "****************** Ensemble"
        "Metrics Ensemble:\n{}\nBest params:\n{}\nRun time:{}"
        "****************** Selected best individual classifier"
        "Index: {}\nScore: {}\nClass: {}\n"
    ).format(
        svc_metrics, svc_best_params, run_time_svm,
        gbt_metrics, gbt_best_params, run_time_gbt,
        eclf_metrics, eclf_best_params, run_time_ensemble,
        max_score_index, best_score, best_classifier
    )

    print(msg)
    if save_output:
        save_outputs.save_output_file(msg, 'results_grid_class_and_ensemble_preprocessed_data_feature_drop', '../doc')

    # train individual best performer and ensemble in the whole training data set:
    best_classifier.fit(X, y)
    eclf_best_estimator.fit(X, y)

    X_test = X_test_preprocessed

    preds_best_classifier = best_classifier.predict(X_test)

    # predict with ensemble classifier
    preds_ensemble = eclf_best_estimator.predict(X_test)

    # submission
    if save_output:
        save_outputs.prepare_submission(df_test_preprocessed.iloc[:, 0], preds_best_classifier,
                                        filename='submission_grid_best_classifier_auto')
        save_outputs.prepare_submission(df_test_preprocessed.iloc[:, 0], preds_ensemble,
                                        filename='submission_grid_ensemble_classifier_auto')

    # save classifiers
    if save_output:
        # save_outputs.save_estimator_pckl(best_classifier, 'grid_auto', 'selected_best')
        save_outputs.save_estimator(best_classifier, 'classifiers', 'grid_auto_selected_best')
        # save_outputs.save_estimator_pckl(eclf_best_estimator, 'grid_auto', 'ensemble')
        save_outputs.save_estimator(best_classifier, 'classifiers', 'grid_auto_ensemble')

    # test if save file and pickle are ok
    save_outputs.prepare_submission(df_test_preprocessed.iloc[:, 0], preds_best_classifier,
                                    filename='submission_grid_best_classifier_auto', base_dir='../..')
    # save_outputs.save_estimator_pckl(best_classifier, 'grid_auto', 'selected_best')
    save_outputs.save_estimator(best_classifier, 'classifiers', 'grid_auto_selected_best', '../../estimators')
