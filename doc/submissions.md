# 1 - Baseline submission
* Metrics obtained in validation:
* Score: 0.61609
* SVC. Basic pre-processing (normalization only).
* (script: [preliminary_analysis.ipynb](../notebooks/preliminary_analysis.ipynb))
* (model: NA)

# 1 - Baseline submission - gradient boosting
* Metrics obtained in validation:
* Score: 0.58459
* GBT. Basic pre-processing (normalization only).
* (script: [experiment010](../src/experiment010.py))
* (model: NA)

# 2 - Submission - gradient boosting with undersampling of majority class
* Metrics obtained in validation:
* Score: 0.77903
* GBT. Basic pre-processing (normalization only). Undersampling majority class.
* (script: [experiment007](../src/experiment007.py))
* (model: NA)

# 3 - Submission - gradient boosting with oversampling of minority class
* Metrics obtained in validation:
* Score: 0.72342
* GBT. Basic pre-processing (normalization only). Oversampling majority class.
* (script: [experiment008](../src/experiment008.py))
* (model: NA)

# 4 - Submission - voting ensemble (gradient boosting + svm) with undersampling of majority class
* Metrics obtained in validation:
* Score: 0.79154
* GBT. Basic pre-processing (normalization only). Undersampling of majority class.
* (script: [experiment008](../src/experiment011.py))
* (model: NA)