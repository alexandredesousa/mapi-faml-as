Alexandre de Sousa

30 Nov. 2021
# Project
For the application project of Foundations and Applications of Machine Learning I propose the analysis of the dataset -
**Santander Customer Transaction Prediction**, publicly available in the
[Kaggle platform](https://www.kaggle.com/c/santander-customer-transaction-prediction/overview).

This data set was provided by an international bank, for competition purposes, specifically, a classification problem.
According to the information provided, the data provided has the same structure of the real data used to solve this problem in the bank.
The question raised by the bank is “identify which customers will make a specific transaction in the future,
irrespective of the amount of money transacted”.
The labeled data set provided (to be used for training and validation) contains 200000 records and 202 variables
(includes target and ID variables); the unlabeled data set also contains 200000 records and 201 variables (includes
the ID variable). All the feature variables are numeric. Not many more details regarding the data are provided. 
The competition is no longer being held, but in case it is still possible to make submissions in the platform, the test set.

# Proposed Analysis
In terms of analysis I intend to address the following questions:
* First, and foremost, is it possible to perform the proposed classification task with the data provided. 
* Which type of model is best suited for the classification task - individual (in which case which) or ensemble.
* Are deep learning techniques viable in this setting (i.e. compared to methods being tested as per previous point).
* What are the most relevant data processing steps for the data provided.
* Is it possible to determine the most relevant features.

To address those questions:
* Evaluate promising models under other metrics that may be relevant besides the one provided in the competition (area under the ROC). 
* Evaluate that the design models are not under or over fitting.
* Besides numerical variables treatment:
  * Evaluate uninformative or redundant features;
  * Filter/select irrelevant features.
  * Identify and treat outliers.
* I am assuming the data set may be unbalanced, as such I intend to evaluate some techniques that allow to overcome the unbalance problem. 

# Note
Personally, I have interest in this data set as in my company I am preparing a Data Science project, and this data set
was selected for the demo.