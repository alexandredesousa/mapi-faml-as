# Comparison formats: csv, pickle, parquet, feather

## save and load times and file size for unpivoting tables

|dataframe|type|size|time to save|time to load|
|---|---|---|---|---|
|train|csv|(1,18 GB em disco)|0:02:22.103391|0:00:45.114195|
|test|csv|(1,06 GB em disco)|0:01:46.662577|0:00:23.183936|
|train|pickle|(1,04 GB em disco)|0:00:15.679333|0:00:04.504148|
|test|pickle|(721,8 MB em disco)|0:00:09.843085|0:00:04.016911|
|train|parquet|(238,4 MB em disco)|0:00:36.416227|0:00:13.760043|
|test|parquet|(237,3 MB em disco)|0:00:31.158581|0:00:13.681945|
|train|feather|(734,7 MB em disco)|0:00:12.143343|0:00:09.531386|
|test|feather|(712,5 MB em disco)|0:00:08.730072|0:00:09.820683|


# Output
```
time to save train data mode = csv: 0:02:22.103391
time to save test data mode = csv: 0:01:46.662577
time to save train data mode = pickle: 0:00:15.679333
time to save test data mode = pickle: 0:00:09.843085
time to save train data mode = parquet: 0:00:36.416227
time to save test data mode = parquet: 0:00:31.158581
time to save train data mode = feather: 0:00:12.143343
time to save test data mode = feather: 0:00:08.730072
time to load train data mode = csv: 0:00:45.114195
time to load test data mode = csv: 0:00:23.183936
time to load train data mode = pickle: 0:00:04.504148
time to load test data mode = pickle: 0:00:04.016911
time to load train data mode = parquet: 0:00:13.760043
time to load test data mode = parquet: 0:00:13.681945
time to load train data mode = feather: 0:00:09.531386
time to load test data mode = feather: 0:00:09.820683
```

# Decision
Decided to use **parquet**. Despite taking longer than pickle and feather to save and load, it is faster way faster than
csv, and saves a considerable amount of space in disk.